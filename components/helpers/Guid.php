<?php
namespace app\components\helpers;

/**
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * Class csvReader
 */
class Guid {
    
    /**
     * Generate Globally Unique Identifier (GUID)
     * E.g. 2EF40F5A-ADE8-5AE3-2491-85CA5CBD6EA7
     *
     * @param boolean $includeBraces
     * Set to `true` if the final Guid needs to be wrapped in curly braces
     * 
     * @return string Guid
     */
    public static function generateGuid($includeBraces = false)
    {
        if (function_exists('com_create_guid')) {
            if ($includeBraces === true) {
                return com_create_guid();
            } else {
                return substr(com_create_guid(), 1, 32);
            }
        } else {
            mt_srand((double) microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));

            $guid = substr($charid, 0, 8) . '-' .
                substr($charid, 8, 4) . '-' .
                substr($charid, 12, 4) . '-' .
                substr($charid, 16, 4) . '-' .
                substr($charid, 20, 8);

            if ($includeBraces) {
                $guid = '{' . substr($guid, 1, 31) . '}';
            }

            return $guid;
        }
    }

}