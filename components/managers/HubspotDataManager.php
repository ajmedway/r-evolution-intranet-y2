<?php

namespace app\components\managers;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Class HubspotDataManager
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * @see https://github.com/ryanwinchester/hubspot-php
 */
class HubspotDataManager extends Component
{

    /**
     * @var string Hubspot API Contact Type
     */
    public static $HSTYPE_CONTACT = 'contact';

    /**
     * @var string Hubspot API Company Type
     */
    public static $HSTYPE_COMPANY = 'company';

    /**
     * @var string Hubspot API Company Type
     */
    public static $HSTYPE_DEAL = 'deal';

    /**
     * @var string Hubspot API Key
     * @see https://developers.hubspot.com/docs/methods/auth/oauth-overview
     */
    public $hubspotApiKey;

    /**
     * @var \SevenShores\Hubspot\Factory Hubspot API factory object
     */
    private $_apiFactory;

    /**
     * @var array Hubspot contact properties
     */
    private $_contactPropertiesList;

    /**
     * @var array Hubspot company properties
     */
    private $_companyPropertiesList;

    /**
     * @var array Hubspot company properties
     */
    private $_dealPropertiesList;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if (empty($this->hubspotApiKey)) {
            throw new InvalidConfigException('Hubspot API Key cannot be empty.');
        }

        parent::init();

        // initialise Hubspot factory instance after configuration is applied
        $this->_apiFactory = $this->getHubspotApiFactoryInstance();
    }

    /**
     * Get Hubspot API factory instance
     * 
     * @return \SevenShores\Hubspot\Factory
     */
    private function getHubspotApiFactoryInstance()
    {
        return new \SevenShores\Hubspot\Factory([
            'key' => $this->hubspotApiKey,
            'oauth' => false, // default
            'base_url' => 'https://api.hubapi.com' // default
        ]);
    }

    /**
     * Get the loaded Hubspot API factory instance
     * 
     * @return \SevenShores\Hubspot\Factory
     */
    public function getApiFactory()
    {
        return $this->_apiFactory;
    }

    /**
     * Callable for extrapolating and normalising the value of a Hubspot entity
     * property
     * 
     * @param object $entity A returned Hubspot entity object
     * @param string $property Property name
     * @return mixed The normalised field value
     */
    public function pVal($entity, $property)
    {
        if (isset($entity->properties->{$property}->value)) {
            $val = $entity->properties->{$property}->value;
            // convert boolean strings to full booleans
            if (is_string($val) && $val === 'true') {
                return true;
            } else if (is_string($val) && $val === 'false') {
                return false;
            }
            return $val;
        }

        return null;
    }

    /**
     * Get all active company properties
     *
     * @return array Company properties
     * 
     * @see https://developers.hubspot.com/docs/methods/companies/get_company_properties
     */
    public function getCompanyPropertiesList()
    {
        // if not cached in this instance, build from API request
        if (!isset($this->_companyPropertiesList)) {
            $companyPropertiesList = [];

            try {
                $response = $this->_apiFactory->companyProperties()->all();
                if ($response->getStatusCode() === 200) {
                    foreach ($response->getData() as $companyProperty) {
                        // exclude deleted properties
                        if ($companyProperty->deleted) {
                            continue;
                        }
                        $companyPropertiesList[] = $companyProperty->name;
                    }
                }
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), __METHOD__);
            }

            $this->_companyPropertiesList = $companyPropertiesList;
        }

        return $this->_companyPropertiesList;
    }

    /**
     * Get all active contact properties
     *
     * @return array Contact properties
     * 
     * @see https://developers.hubspot.com/docs/methods/contacts/v2/get_contacts_properties
     */
    public function getContactPropertiesList()
    {
        // if not cached in this instance, build from API request
        if (!isset($this->_contactPropertiesList)) {
            $contactPropertiesList = [];

            try {
                $response = $this->_apiFactory->contactProperties()->all();
                if ($response->getStatusCode() === 200) {
                    foreach ($response->getData() as $contactProperty) {
                        // exclude deleted properties
                        if ($contactProperty->deleted) {
                            continue;
                        }
                        $contactPropertiesList[] = $contactProperty->name;
                    }
                }
            } catch (\Exception $e) {
                // log error
                // $e->getMessage();
            }

            $this->_contactPropertiesList = $contactPropertiesList;
        }

        return $this->_contactPropertiesList;
    }

    /**
     * Get all active deal properties
     *
     * @return array Deal properties
     * 
     * @see https://developers.hubspot.com/docs/methods/deals/get_deal_properties
     */
    public function getDealPropertiesList()
    {
        // if not cached in this instance, build from API request
        if (!isset($this->_dealPropertiesList)) {
            $dealPropertiesList = [];

            try {
                $response = $this->_apiFactory->dealProperties()->all();
                if ($response->getStatusCode() === 200) {
                    foreach ($response->getData() as $dealProperty) {
                        // exclude deleted properties
                        if ($dealProperty->deleted) {
                            continue;
                        }
                        $dealPropertiesList[] = $dealProperty->name;
                    }
                }
            } catch (\Exception $e) {
                // log error
                // $e->getMessage();
            }

            $this->_dealPropertiesList = $dealPropertiesList;
        }

        return $this->_dealPropertiesList;
    }

    /**
     * Get a specific company by id
     *
     * @param integer $id Hubspot company Id
     * @return mixed
     * 
     * @see https://developers.hubspot.com/docs/methods/companies/get_company
     */
    public function getCompanyById($id)
    {
        $params = [
            'property' => $this->getCompanyPropertiesList(),
        ];

        try {
            $response = $this->_apiFactory->companies()->getById($id);
            return ($response->getStatusCode() === 200) ? $response->getData() : false;
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), __METHOD__);
        }

        return false;
    }

    /**
     * Get all contacts
     *
     * @param int $count Count records to return
     * @param int $offset Start vid (Hubspot user id)
     * @return array Contacts
     * 
     * @see https://developers.hubspot.com/docs/methods/contacts/get_contacts
     */
    public function getAllContacts($count = 100, $offset = 0)
    {
        $contacts = [];
        $params = [
            'count' => $count,
            'property' => $this->getContactPropertiesList(),
        ];
        $i = 0;

        // Iteratively get all contacts (from start vid if set)
        do {
            $i++;
            if (!empty($response->data->{'vid-offset'})) {
                $params['vidOffset'] = $response->data->{'vid-offset'};
            } elseif ($i === 1 && !empty($offset)) {
                $params['vidOffset'] = $offset;
            }

            try {
                $response = $this->_apiFactory->contacts()->all($params);
                if (($response->getStatusCode() === 200)) {
                    $contacts = array_merge($contacts, $response->data->contacts);
                }
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), __METHOD__);
            }
        } while (!empty($response->data->{'hasMore'}));

        return $contacts;
    }

    /**
     * Get a specific contact by email
     *
     * @param string $email
     * @return mixed
     * 
     * @see https://developers.hubspot.com/docs/methods/contacts/get_contact_by_email
     */
    public function getContactByEmail($email)
    {
        $params = [
            'property' => $this->getContactPropertiesList(),
        ];

        try {
            $response = $this->_apiFactory->contacts()->getByEmail($email, $params);
            return ($response->getStatusCode() === 200) ? $response->getData() : false;
        } catch (\Exception $e) {
            // log error
            // $e->getMessage();
        }

        return false;
    }

    /**
     * Get all deals
     *
     * @param int $limit The number of records to return. Defaults to 100, has a
     * maximum value of 250.
     * @param int $offset Used to page through the results. If there are more
     * records in your portal than the limit= parameter, you will need to use
     * the offset returned in the first request to get the next set of results.
     * @return array Deals
     * 
     * @see https://developers.hubspot.com/docs/methods/deals/get-all-deals
     */
    public function getAllDeals($limit = 250, $offset = 0)
    {
        $deals = [];
        $params = [
            'limit' => $limit,
            'properties' => $this->getDealPropertiesList(),
        ];
        $i = 0;

        // Iteratively get all deals (from start dealId if set)
        do {
            $i++;
            if (!empty($response->data->{'offset'})) {
                $params['offset'] = $response->data->{'offset'};
            } elseif ($i === 1 && !empty($offset)) {
                $params['offset'] = $offset;
            }

            try {
                $response = $this->_apiFactory->deals()->getAll($params);
                if (($response->getStatusCode() === 200)) {
                    $deals = array_merge($deals, $response->data->deals);
                }
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), __METHOD__);
            }
        } while (!empty($response->data->{'hasMore'}));

        return $deals;
    }

    /**
     * Get all deals
     *
     * @param int $dealId The deal ID.
     * @param int $properties The deal properties to update.
     * @return bool Success
     * 
     * @see https://developers.hubspot.com/docs/methods/deals/update_deal
     */
    public function updateDealById($dealId, $properties)
    {
        try {
            $response = $this->_apiFactory->deals()->update($dealId, ['properties' => $properties]);
            if (($response->getStatusCode() === 200)) {
                return true;
            }
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), __METHOD__);
        }

        return false;
    }

}
