<?php
return [
    'createClient' => [
        'type' => 2,
        'description' => 'Create a client',
    ],
    'updateClient' => [
        'type' => 2,
        'description' => 'Update client',
    ],
    'deleteClient' => [
        'type' => 2,
        'description' => 'Delete client',
    ],
    'createJob' => [
        'type' => 2,
        'description' => 'Create a job',
    ],
    'updateJob' => [
        'type' => 2,
        'description' => 'Update job',
    ],
    'deleteJob' => [
        'type' => 2,
        'description' => 'Delete job',
    ],
    'createPerson' => [
        'type' => 2,
        'description' => 'Create a person',
    ],
    'updatePerson' => [
        'type' => 2,
        'description' => 'Update person',
    ],
    'deletePerson' => [
        'type' => 2,
        'description' => 'Delete person',
    ],
    'user' => [
        'type' => 1,
        'children' => [
            'createClient',
            'updateClient',
            'createJob',
            'updateJob',
            'deleteJob',
        ],
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'user',
            'deleteClient',
            'createPerson',
            'updatePerson',
            'deletePerson',
        ],
    ],
];
