<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PersonOnJob */

$this->title = 'Create Person On Job';
$this->params['breadcrumbs'][] = ['label' => 'People On Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-on-job-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
