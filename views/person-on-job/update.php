<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PersonOnJob */

$this->title = 'Update Person On Job: ' . $model->person_on_job_id;
$this->params['breadcrumbs'][] = ['label' => 'People On Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->person_on_job_id, 'url' => ['view', 'id' => $model->person_on_job_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="person-on-job-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
