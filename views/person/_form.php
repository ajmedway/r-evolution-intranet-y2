<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Person;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="person-form">

    <?php $form = ActiveForm::begin(); ?>
    
    	<div class="row hidden">
            <div class="col-sm-6">
                <?= $form->field($model, 'guid')->textInput(['maxlength' => true, 'readonly'=>'readonly', 'disabled'=>'disabled']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
            	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'team')->dropDownList(Person::teams(true)) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <label>Is Team Lead?</label>
                <?= $form->field($model, 'team_lead')->checkbox(['label' => 'Yes']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <label>Is Person Active?</label>
                <?= $form->field($model, 'active')->checkbox(['label' => 'Yes']) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-revs' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
