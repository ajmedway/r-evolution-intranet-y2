<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\Person;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'People';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if (\Yii::$app->user->can('createPerson')): ?>
        <p>
            <?= Html::a('Create Person', ['create'], ['class' => 'btn btn-revs']) ?>
        </p>
    <?php endif; ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'vAlign' => 'middle',
            ],
            [
                'attribute' => 'person_id',
                'vAlign' => 'middle',
            ],
            [
                // Person name (with editable popover)
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'name',
                'vAlign' => 'middle',
                'width' => '220px',
                'editableOptions' => [
                    // controller action to dispatch the updates to
                    'formOptions' => ['action' => ['person/pjax-edit']],
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                ],
            ],
            [
                // state with states dropdown (with editable popover)
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'team',
                'vAlign' => 'middle',
                'width' => '200px',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Person::teams(true),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Any Team'],
                'format' => 'raw',
                'editableOptions' => [
                    'size' => 'md',
                    'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                    'formOptions' => ['action' => ['person/pjax-edit']], // controller action to dispatch the updates to
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => Person::teams(true),
                ],
            ],
            [
                // team_lead (with editable popover)
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'team_lead',
                'vAlign' => 'middle',
                'width' => '120px',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [0 => 'No', 1 => 'Yes'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Either'],
                'editableOptions' => [
                    'displayValueConfig' => [0 => 'No', 1 => 'Yes'],
                    'size' => 'md',
                    'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                    'formOptions' => ['action' => ['person/pjax-edit']], // controller action to dispatch the updates to
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => [0 => 'No', 1 => 'Yes'],
                ],
            ],
            [
                // active (with editable popover)
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'active',
                'vAlign' => 'middle',
                'width' => '120px',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [0 => 'No', 1 => 'Yes'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Either'],
                'editableOptions' => [
                    'displayValueConfig' => [0 => 'No', 1 => 'Yes'],
                    'size' => 'md',
                    'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                    'formOptions' => ['action' => ['person/pjax-edit']], // controller action to dispatch the updates to
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => [0 => 'No', 1 => 'Yes'],
                ],
            ],
            [
                'attribute' => 'date_created',
                'vAlign' => 'middle',
                'format' => 'date',
            ],
            [
                'attribute' => 'date_modified',
                'vAlign' => 'middle',
                'format' => 'date',
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'vAlign' => 'middle',
                'viewOptions' => ['title' => 'View Job details', 'data-toggle' => 'tooltip'],
                'updateOptions' => ['title' => 'Edit Job details', 'data-toggle' => 'tooltip'],
                'deleteOptions' => ['title' => 'Delete this Job?', 'data-toggle' => 'tooltip'],
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'visibleButtons' => [
                    'delete' => \Yii::$app->user->can('deletePerson'),
                    'update' => \Yii::$app->user->can('updatePerson'),
                ],
            ],
        ],
    ]);
    ?>

</div>
