<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'r//evolution Intranet';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><span class="revorange">r//evolution</span> Intranet</h1>

        <p class="lead">In-house toolkit for tracking staff, clients, jobs and billings.</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h2><?= $title ?></h2>
                <p><?= $message ?></p>
            </div>
        </div>

    </div>
</div>
