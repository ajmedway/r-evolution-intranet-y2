<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jobs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <?php if (\Yii::$app->user->can('createJob')): ?>
        <p>
            <?= Html::a('Create Job', ['create'], ['class' => 'btn btn-revs']) ?>
        </p>
    <?php endif; ?>

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'job_id',
            'hs_deal_id',
            // 'guid',
            // 'client_id',
            'client.name:ntext:Client',
            'job_name:ntext',
            'quote_rate:currency',
            'quote_unit:ntext',
            // 'total_units',
            // 'total_value',
            // 'sale_total_value',
            // 'in_cost',
            'margin:currency',
            //'project_manager_id:ntext',
            'projectManager.name:ntext:Project Manager',
            // 'state:ntext',
            // 'expected_billing_date',
            // 'feedback_sent',
            // 'date_created',
            // 'date_modified',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'visibleButtons' => [
                    'delete' => \Yii::$app->user->can('deleteJob'),
                    'update' => \Yii::$app->user->can('updateJob'),
                ],
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>

</div>
