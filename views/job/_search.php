<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JobSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'job_id') ?>

    <?= $form->field($model, 'guid') ?>

    <?= $form->field($model, 'hs_deal_id') ?>

    <?= $form->field($model, 'client_id') ?>

    <?= $form->field($model, 'job_name') ?>

    <?= $form->field($model, 'quote_rate') ?>

    <?php // echo $form->field($model, 'quote_unit') ?>

    <?php // echo $form->field($model, 'total_units') ?>

    <?php // echo $form->field($model, 'total_value') ?>

    <?php // echo $form->field($model, 'sale_total_value') ?>

    <?php // echo $form->field($model, 'in_cost') ?>

    <?php // echo $form->field($model, 'margin') ?>

    <?php // echo $form->field($model, 'project_manager_id') ?>
    
    <?php // echo $form->field($model, 'created_by_id') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'expected_billing_date') ?>

    <?php // echo $form->field($model, 'feedback_sent') ?>

    <?php // echo $form->field($model, 'date_created') ?>

    <?php // echo $form->field($model, 'date_modified') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
