<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\detail\DetailView;
use app\models\Job;
use app\models\Client;
use app\models\Person;
use app\models\PersonOnJob;

/* @var $this yii\web\View */
/* @var $job app\models\Job */
/* @var $form yii\widgets\ActiveForm */

/**
 * See - http://demos.krajee.com/detail-view-demo
 *     - http://demos.krajee.com/detail-view
 */

// DetailView Attributes Configuration
$attributes = [
    [
        'group' => true,
        'label' => 'Job Information',
        'rowOptions' => ['class' => 'grey']
    ],
    [
        'columns' => [
            [
                'attribute' => 'quote_rate',
                'format' => 'currency',
                'valueColOptions' => ['style' => 'width:15%'],
            ],
            [
                'attribute' => 'quote_unit',
                'format' => 'ntext',
                'valueColOptions' => ['style' => 'width:15%'],
            ],
            [
                'attribute' => 'total_units',
                'format' => ['decimal', 2],
                'valueColOptions' => ['style' => 'width:15%'],
            ],
            [
                'attribute' => 'job_id',
                'label' => 'Relationship Manager',
                'value' => $job->client->relationshipManager->name,
                'valueColOptions' => ['style' => 'width:15%'],
            ],
        ],
    ],
    [
        'group' => true,
        'label' => 'People On Job',
        'rowOptions' => ['class' => 'grey']
    ],
];

// append person on job data
foreach ($job->personOnJobs as $personOnJob) {
    $attributes[]['columns'] = [
        [
            'attribute' => 'job_id',
            'label' => 'Name',
            'value' => $personOnJob->person->name,
            'valueColOptions' => ['style' => 'width:15%'],
        ],
        [
            'attribute' => 'job_id',
            'label' => 'Team',
            'value' => $personOnJob->person->team,
            'valueColOptions' => ['style' => 'width:15%'],
        ],
        [
            'attribute' => 'job_id',
            'label' => 'Time Units',
            'value' => $personOnJob->time_units,
            'valueColOptions' => ['style' => 'width:15%'],
        ],
        [
            'attribute' => 'job_id',
            'label' => 'Calculated Value',
            'value' => $personOnJob->time_units * $job->quote_rate,
            'format' => 'currency',
            'valueColOptions' => ['style' => 'width:15%'],
        ],
    ];
}

// View file rendering the widget
echo DetailView::widget([
    'model' => $job,
    'attributes' => $attributes,
    'mode' => 'view',
    'bordered' => true,
    'striped' => false,
    'condensed' => true,
    'responsive' => false,
    'hover' => false,
    'hAlign' => DetailView::ALIGN_RIGHT,
    'vAlign' => DetailView::ALIGN_MIDDLE,
    'fadeDelay' => 800,
    'labelColOptions' => ['style' => 'width:10%'],
    'options' => ['type' => 'default', 'style' => 'margin-bottom:0px;'],
]);
