<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\dialog\Dialog;
use app\components\helpers\Template;
use app\models\Job;
use app\models\Client;
use app\models\Person;
use app\models\PersonOnJob;

// get data for populating the form/dropdown options etc.
$people = Person::find()->peopleTeamsOptions()->asArray()->all();
$peopleOptions = ArrayHelper::map($people, 'person_id', 'name_team', 'type');

$clients = Client::find()->orderBy('name')->all();
$clientOptions = ArrayHelper::map($clients, 'client_id', 'name');

// get model instance for creating the People On Job activeform fields
$personOnJob = new PersonOnJob();
$personOnJob->loadDefaultValues();

/* @var $this yii\web\View */
/* @var $job app\models\Job */
/* @var $form yii\widgets\ActiveForm */

?>

<?php if ($job->hasErrors()): ?>
    <?php foreach ($job->getErrors() as $error): ?>
        <div class="alert alert-danger">
            <strong>Error:</strong> <?= print_r($error, true) ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

<div class="job-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($job, 'guid')->textInput(['maxlength' => true, 'readonly' => 'readonly', 'disabled' => 'disabled']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($job, 'hs_deal_id')->textInput(['maxlength' => true, 'readonly' => 'readonly', 'disabled' => 'disabled']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($job, 'state')->dropDownList(Job::states(true), ['prompt' => 'Job State']) ?>
        </div>
        <div class="col-sm-3">
            <label>Feedback Left?</label>
            <?= $form->field($job, 'feedback_sent')->checkbox(['label' => 'Yes']) ?>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($job, 'job_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($job, 'client_id')->dropDownList($clientOptions, ['prompt' => 'Select Client']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($job, 'project_manager_id')->dropDownList($peopleOptions, ['prompt' => 'Select Project Manager']) ?>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($job, 'quote_unit')->dropDownList(Job::timeUnits(true), ['prompt' => 'Quote Unit']) ?>
        </div>
        <div class="col-sm-3">
            <?=
            $form->field($job, 'quote_rate', [
                'template' => Template::getTextInputAddonTemplate('glyphicon-gbp', true)
            ])->textInput(['type' => 'float', 'maxlength' => 6, 'class' => 'form-control numericFloatKeysOnly'])->label('Quote Rate (Per Unit)')
            ?>
        </div>
        <div class="col-sm-3">
            <?=
            $form->field($job, 'total_units', [
                'template' => Template::getTextInputAddonTemplate('glyphicon-time', true)
            ])->textInput(['type' => 'float', 'maxlength' => 6, 'readonly' => 'readonly', 'class' => 'form-control numericFloatKeysOnly'])
            ?>
        </div>
        <div class="col-sm-3">
            <?=
            $form->field($job, 'in_cost', [
                'template' => Template::getTextInputAddonTemplate('glyphicon-gbp', true)
            ])->textInput(['type' => 'float', 'maxlength' => 6, 'class' => 'form-control numericFloatKeysOnly'])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?=
            $form->field($job, 'expected_billing_date')->widget(DatePicker::classname(), [
                //'value' => date('Y-m-d', strtotime('+30 days')),
                'options' => ['placeholder' => 'Select billing date...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?=
            $form->field($job, 'sale_total_value', [
                'template' => Template::getTextInputAddonTemplate('glyphicon-gbp', true)
            ])->textInput(['type' => 'float', 'maxlength' => 6, 'class' => 'form-control numericFloatKeysOnly'])
            ?>
        </div>
        <div class="col-sm-3">
            <?=
            $form->field($job, 'total_value', [
                'template' => Template::getTextInputAddonTemplate('glyphicon-gbp', true)
            ])->textInput(['type' => 'float', 'maxlength' => 6, 'readonly' => 'readonly', 'class' => 'form-control numericFloatKeysOnly'])
            ?>
        </div>
        <div class="col-sm-3">
            <?=
            $form->field($job, 'margin', [
                'template' => Template::getTextInputAddonTemplate('glyphicon-gbp', true)
            ])->textInput(['type' => 'float', 'maxlength' => 6, 'readonly' => 'readonly', 'class' => 'form-control numericFloatKeysOnly'])
            ?>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-sm-4">
            <?= $form->field($job, 'date_created')->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($job, 'date_modified')->textInput() ?>
        </div>
    </div>

    <!-- PEOPLE ON JOB -->

    <div class="panel panel-default people-on-job-panel">
        <div class="panel-heading">
            <h3 class="panel-title">People On Job</h3>
        </div>
        <div class="panel-body">
            <div class="people-on-job-wrapper">
                <div class="person-on-job-rows">
                    <?php if ($job->personOnJobs): ?>

                        <?php
                        // populate the form with existing People On Job records
                        $personCount = 0;
                        foreach ($job->personOnJobs as $person):
                            ?>
                            <div class="row person_on_job" id="person_on_job_<?php echo($personCount); ?>">
                                <span class="wrapper-person_on_job_id">
                                    <?=
                                    $form->field($personOnJob, "[{$personCount}]person_on_job_id")->hiddenInput([
                                        'value' => $person->person_on_job_id,
                                        'readonly' => true,
                                        'class' => 'form-control person_on_job_id'
                                    ])->label(false)
                                    ?>
                                </span>
                                <div class="col-sm-6">
                                    <?=
                                    $form->field($personOnJob, "[{$personCount}]person_id")->dropDownList($peopleOptions, [
                                        'value' => $person->person_id,
                                        'prompt' => 'Team member',
                                        'class' => 'form-control person_id'
                                    ])
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?=
                                    $form->field($personOnJob, "[{$personCount}]time_units", [
                                        'template' => Template::getTextInputAddonTemplate('glyphicon-time', true)
                                    ])->textInput([
                                        'value' => $person->time_units,
                                        'type' => 'float',
                                        'maxlength' => 6,
                                        'class' => 'form-control time_units numericFloatKeysOnly'
                                    ])
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group control-remove-person">
                                        <label class="control-label hidden-xs">&nbsp;</label>
                                        <div class="remove-person">
                                            <a role="button" class="btn btn-danger" href="#" title="Delete" aria-label="Delete">
                                                <span class="glyphicon glyphicon-trash"></span> Remove Person
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $personCount++;
                        endforeach;
                        ?>

                    <?php else: ?>

                        <div class="row person_on_job" id="person_on_job_0">
                            <div class="col-sm-6">
                                <?=
                                $form->field($personOnJob, '[0]person_id')->dropDownList($peopleOptions, [
                                    'prompt' => 'Team member',
                                    'class' => 'form-control person_id'
                                ])
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <?=
                                $form->field($personOnJob, '[0]time_units', [
                                    'template' => Template::getTextInputAddonTemplate('glyphicon-time', true)
                                ])->textInput([
                                    'type' => 'float',
                                    'maxlength' => 6,
                                    'class' => 'form-control time_units numericFloatKeysOnly'
                                ])
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group control-remove-person">
                                    <label class="control-label hidden-xs">&nbsp;</label>
                                    <div class="remove-person">
                                        <a role="button" class="btn btn-danger" href="#" title="Delete" aria-label="Delete">
                                            <span class="glyphicon glyphicon-trash"></span> Remove Person
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endif; ?>
                </div>
                <div class="row add-person-row">
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-revs add-person"><span class="glyphicon glyphicon-plus-sign"></span> Add Person</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- CLOSE PEOPLE ON JOB -->

    <div class="row">
        <div class="col-sm-4 hidden">
            <?= $form->field($job, 'date_created')->textInput(['readonly' => 'readonly', 'disabled' => 'disabled']) ?>
        </div>
        <div class="col-sm-4 hidden">
            <?= $form->field($job, 'date_modified')->textInput(['readonly' => 'readonly', 'disabled' => 'disabled']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($job->isNewRecord ? '<span class="glyphicon glyphicon-plus"></span> Create' : '<span class="glyphicon glyphicon-edit"></span> Update', ['class' => $job->isNewRecord ? 'btn btn-lg btn-revs' : 'btn btn-lg btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php
//    // Example 1
//    echo Dialog::widget([
//       'libName' => 'krajeeDialog',
//       'options' => [], // default options
//    ]);
//
//    // Example 2
//    echo Dialog::widget([
//       'libName' => 'krajeeDialogCust',
//       'options' => ['draggable' => true, 'closable' => true], // custom options
//    ]);
    ?>

</div>
