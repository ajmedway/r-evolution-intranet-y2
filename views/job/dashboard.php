<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\Job;
use app\models\Client;
use app\models\Person;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProviderCommunicationsBreakdown yii\data\ActiveDataProvider */
/* @var $dataProviderDesignBreakdown yii\data\ActiveDataProvider */
/* @var $dataProviderDigitalBreakdown yii\data\ActiveDataProvider */
/* @var $dataProviderOLMBreakdown yii\data\ActiveDataProvider */
/* @var $dataProviderProjectManagementBreakdown yii\data\ActiveDataProvider */
/* @var $dataProviderOtherBreakdown yii\data\ActiveDataProvider */

/**
 * See - http://demos.krajee.com/grid-demo
 *     - http://demos.krajee.com/grid
 */

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="job-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="panel-group" id="accordion-teamBreakdowns" role="tablist" aria-multiselectable="true">
    
        <?php foreach (Person::teams() as $key => $teamName):
            $headingTeamBreakdown = '<i class="glyphicon glyphicon-book"></i> <span class="revorange">r//evolution</span> ' . $teamName . ' Breakdown';
            $camelisedTeamName = \yii\helpers\Inflector::camelize($teamName);
            $teamDataProviderName = "dataProvider{$camelisedTeamName}Breakdown";
            ?>
            <?=
            GridView::widget([
                'dataProvider' => $$teamDataProviderName,
                //'filterModel' => $searchModel,
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => ['id' => "pjax-grid-{$camelisedTeamName}"]
                ],
                'condensed' => true,
                'striped' => false,
                'hover' => false,
                'tableOptions' => ['class' => 'breakdown-data no-details'],
                'panel' => [
                    'type' => GridView::TYPE_DEFAULT,
                    'heading' => '
                            <a role="button" data-toggle="collapse" data-parent="#accordion-teamBreakdowns" href="#collapse' . $camelisedTeamName . '" aria-expanded="false" aria-controls="collapse' . $camelisedTeamName . '">
                                ' . $headingTeamBreakdown . '
                            </a>',
                    'headingOptions' => ['class' => 'panel-heading', 'id' => "heading{$camelisedTeamName}"],
                ],
                'panelTemplate' => '
                            <div class="breakdown-data">
                                {panelHeading}
                                <div id="collapse' . $camelisedTeamName . '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' . $camelisedTeamName . '">
                                    <div class="panel-body">
                                        {panelBefore}
                                        {items}
                                    </div>
                                </div>
                            </div>',
                'toolbar' =>  [
                    [
                        'content' => '
                            <button type="button" class="btn btn-default btn-details-toggle" id="details-toggle-' . $camelisedTeamName . '" data-toggle="button" aria-pressed="false" autocomplete="off">
                                <span class="glyphicon glyphicon-list" aria-hidden="true"></span> Details toggle
                            </button>'
                    ],
                ],
                'showPageSummary' => true,
                'pageSummaryRowOptions' => ['class' => 'kv-page-summary default', 'style' => 'font-size:16px;'],
                // <editor-fold defaultstate="collapsed" desc="columns">
                'columns' => [
                    [
                        'attribute' => 'job.state',
                        //'hidden' => true,
                        'vAlign' => 'middle',
                        'hAlign' => 'left',
                        'contentOptions' => ['style' => 'font-weight: bold'],
                        'group' => true,
                        'groupedRow' => true,
                        'groupOddCssClass' => 'kv-grouped-row revs',    // configure odd group cell css class
                        'groupEvenCssClass' => 'kv-grouped-row revs',   // configure even group cell css class
                        'groupFooter' => function ($model, $key, $index, $widget) {
                            return [
                                'mergeColumns' => [[1,4]],              // columns to merge in summary
                                'content' => [                          // content to show in each summary cell
                                    1 => '<span class="glyphicon glyphicon-gbp" aria-hidden="true"></span> Total ' . $model->job->state,
                                    5 => GridView::F_SUM,
                                ],
                                'contentFormats' => [                   // content reformatting for each summary cell
                                    5 => ['format' => 'number', 'decimals' => 2],
                                ],
                                'contentOptions'=>[                     // content html attributes for each summary cell
                                    1 => ['style' => 'font-variant:small-caps'],
                                    5 => ['style' => 'text-align:right'],
                                ],
                                // html attributes for group summary row
                                'options' => [
                                    'class' => 'danger',
                                    'style' => 'font-weight:bold;'
                                ],
                            ];
                        },
                    ],
                    [
                        'attribute' => 'person.name',
                        'vAlign' => 'middle',
                        'hAlign' => 'left',
                        'group' => true,
                        'subGroupOf' => 0,                              // job status column index is the parent group,
                        'groupOddCssClass' => 'kv-group-even',          // configure odd group cell css class
                        'groupEvenCssClass' => 'kv-group-even',         // configure even group cell css class
                        'groupFooter' => function ($model, $key, $index, $widget) { // Closure method
                            return [
                                'mergeColumns' => [[1,4]],              // columns to merge in summary
                                'content' => [                          // content to show in each summary cell
                                    1 => '<span class="glyphicon glyphicon-gbp" aria-hidden="true"></span> Total ' . $model->job->state . ' <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> ' . $model->person->name,
                                    5 => GridView::F_SUM,
                                ],
                                'contentFormats' => [                   // content reformatting for each summary cell
                                    5 => ['format' => 'number', 'decimals' => 2],
                                ],
                                'contentOptions'=> [                    // content html attributes for each summary cell
                                    1 => ['style' => 'font-variant:small-caps'],
                                    5 => ['style' => 'text-align:right'],
                                ],
                                // html attributes for group summary row
                                'options' => [
                                    'class' => 'default',
                                    'style' => 'font-weight:bold;'
                                ],
                            ];
                        },
                    ],
                    [
                        'header' => 'Client &rarr; Job Name',
                        'value' => function ($model, $key, $index, $widget) {
                            return $model->job->client->name . ' &rarr; ' . Html::a(
                                '<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span> ' . $model->job->job_name,
                                ['job/update', 'id' => $model->job->job_id],
                                ['data-pjax' => 0, 'target' => '_blank', 'title' => 'View/Edit Job']
                            );
                        },
                        'vAlign' => 'middle',
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'job.quote_rate',
                        'vAlign' => 'middle',
                        'format' => ['currency'],
                    ],
                    [
                        'attribute' => 'time_units',
                        'vAlign' => 'middle',
                        'format' => ['decimal', 2],
                    ],
                    [
                        'class' => 'kartik\grid\FormulaColumn',
                        'header' => 'Person On Job Total',
                        'value' => function ($model, $key, $index, $widget) {
                            if ($model->person->isTeam() && $model->job->quote_unit === 'Fixed') {
                                // if this personOnJob row is a team AND it has a fixed quote_unit
                                // we are only interested in the pre-calculated job margin
                                return $model->job->margin;
                            } else {
                                // otherwise, work out the quote_rate * time_units
                                $p = compact('model', 'key', 'index');
                                return $widget->col(3, $p) * $widget->col(4, $p);
                            }
                        },
                        'width' => '150px',
                        'hAlign' => 'right',
                        'vAlign' => 'middle',
                        'format' => ['decimal', 2],
                        'pageSummary' => true,
                        //'pageSummaryOptions' => ['prepend' => '&pound;'],
                    ],
                ],
                // </editor-fold>
            ]);
            ?>
        
        <?php endforeach; ?>
        
    </div>
    

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => [
            'options' => ['id' => 'pjax-grid-dashboardMain']
        ],
        'condensed' => true,
        'striped' => true,
        'hover' => false,
        'floatHeader' => true,
        'floatOverflowContainer' => false,
        'floatHeaderOptions' => [
            'debug' => false,
            'autoReflow' => true,
            'position' => 'auto',
            'top' => 50,
        ],
        'panel' => [
            'type' => GridView::TYPE_DEFAULT,
            'heading' => '<i class="glyphicon glyphicon-book"></i> <span class="revorange">r//evolution</span> Dynamic Billings Sheet',
            'headingOptions' => ['class' => 'panel-heading'],
        ],
        'toolbar' =>  [
            ['content' =>
                Html::a('<i class="glyphicon glyphicon-plus"></i> Create Job', ['job/create'], ['data-pjax' => 0, 'type' => 'button', 'title' => 'Create Job', 'class' => 'btn btn-revs'])
            ],
            '{export}',
            //'{toggleData}',
        ],
        'showPageSummary' => true,
        'pageSummaryRowOptions' => ['class' => 'kv-page-summary revs'],
        // <editor-fold defaultstate="collapsed" desc="columns">
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'vAlign' => 'middle',
            ],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'width' => '50px',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function ($model, $key, $index, $column) {
                    return Yii::$app->controller->renderPartial('_expand-row-job-details', ['job' => $model]);
                },
                'detailRowCssClass' => 'default',
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true,
            ],
            [
                // Client name with clients dropdown (with editable popover)
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'client_id',
                'label' => 'Client',
                'vAlign' => 'middle',
                //'width' => '200px',
                'value' => 'client.name',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Client::find()->orderBy('name')->asArray()->all(), 'client_id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'All'],
                'format' => 'raw',
                'editableOptions' => [
                    'header' => 'Client',
                    'size' => 'md',
                    'placement' => \kartik\popover\PopoverX::ALIGN_RIGHT,
                    'formOptions' => ['action' => ['job/pjax-edit']], // controller action to dispatch the updates to
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => ArrayHelper::map(Client::find()->orderBy('name')->asArray()->all(), 'client_id', 'name'),
                ],
            ],
            [
                // Job name (with editable popover)
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'job_name',
                'vAlign' => 'middle',
                'hAlign' => 'left',
                //'width' => '280px',
                'headerOptions' => [
                    'class' => 'kv-sticky-column',
                    //'style' => 'width: 280px; min-width: 280px; max-width: 280px;'
                ],
                'editableOptions' => [
                    'size' => 'md',
                    // controller action to dispatch the updates to
                    'formOptions' => ['action' => ['job/pjax-edit']],
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                ],
            ],
            [
                // Project Manager name with persons dropdown (with editable popover)
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'project_manager_id',
                'label' => 'Project Manager',
                'vAlign' => 'middle',
                //'width' => '200px',
                'value' => 'projectManager.name',
                /*'value' => function ($model, $key, $index, $widget) {
                    return Html::a($model->projectManager->name, '#', ['title' => 'View person detail', 'onclick' => 'alert("This will open the person page.\n\nDisabled for this demo!")']);
                },*/
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Person::find()->orderBy('name')->asArray()->all(), 'person_id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'All'],
                'format' => 'raw',
                'editableOptions' => [
                    'header' => 'Project Manager',
                    'size' => 'md',
                    'placement' => \kartik\popover\PopoverX::ALIGN_RIGHT,
                    'formOptions' => ['action' => ['job/pjax-edit']], // controller action to dispatch the updates to
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => ArrayHelper::map(Person::find()->orderBy('name')->asArray()->all(), 'person_id', 'name'),
                ],
            ],
            [
                'attribute' => 'created_by_id',
                'label' => 'Created By',
                'vAlign' => 'middle',
                'value' => 'createdBy.name',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Person::find()->orderBy('name')->asArray()->all(), 'person_id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'All'],
                'format' => 'raw',
            ],
            [
                'attribute' => 'total_value',
                'vAlign' => 'middle',
                'format' => ['currency'],
                'pageSummary' => true,
            ],
            [
                'attribute' => 'sale_total_value',
                'vAlign' => 'middle',
                'format' => ['currency'],
                'pageSummary' => true,
            ],
            /*[
                'attribute' => 'in_cost',
                'vAlign' => 'middle',
                'format' => ['currency'],
                'pageSummary' => true,
            ],*/
            [
                'attribute' => 'margin',
                'vAlign' => 'middle',
                'format' => ['currency'],
                'pageSummary' => true,
                'contentOptions' => ['style' => 'font-weight: bold'],
            ],
            [
                // state with states dropdown (with editable popover)
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'state',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'width' => '140px',
                'headerOptions' => [
                    'style' => 'width: 140px; min-width: 140px; max-width: 140px;'
                ],
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Job::states(true),
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All'],
                'format' => 'raw',
                'editableOptions' => [
                    'header' => 'State',
                    'size' => 'md',
                    'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                    'formOptions' => ['action' => ['job/pjax-edit']], // controller action to dispatch the updates to
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => Job::states(true),
                ],
            ],
            [
                // state with states dropdown (with editable popover)
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'feedback_sent',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'value' => function ($model, $key, $index, $widget) {
                    return $model->feedback_sent ? 'Yes' : 'No';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [false => 'No', true => 'Yes'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'All'],
                'format' => 'raw',
                'editableOptions' => [
                    'header' => 'State',
                    'size' => 'md',
                    'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                    'formOptions' => ['action' => ['job/pjax-edit']], // controller action to dispatch the updates to
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => [false => 'No', true => 'Yes'],
                ],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'expected_billing_date',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'width' => '240px',
                'format' => ['date', 'dd-MM-yyyy'],
                'xlFormat' => "mmm\\-dd\\, \\-yyyy",
                'headerOptions' => [
                    'class' => 'kv-sticky-column',
                    'style' => 'width: 240px; min-width: 240px; max-width: 240px;'
                ],
                'contentOptions' => ['class' => 'kv-sticky-column'],
                
                # FILTER SETTINGS
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'model' => $searchModel,
                    'presetDropdown' => true,
                    'useWithAddon' => true,
                    'convertFormat' => true,
                    'startAttribute' => 'expectedBillingDateStart',
                    'endAttribute' => 'expectedBillingDateEnd',
                    'pluginOptions' => [
                        'allowClear' => true,
                        'locale' => [
                            'format' => 'd-m-Y',
                        ],
                        'ranges' => [
                            Yii::t('app', 'Today') => ["moment().startOf('day')", 'moment()'],
                            Yii::t('app', 'Yesterday') => [
                                "moment().startOf('day').subtract(1,'days')",
                                "moment().endOf('day').subtract(1,'days')",
                            ],
                            Yii::t('app', 'Last {n} Days', ['n' => 7]) => ["moment().startOf('day').subtract(6, 'days')", 'moment()'],
                            Yii::t('app', 'Last {n} Days', ['n' => 30]) => ["moment().startOf('day').subtract(29, 'days')", 'moment()'],
                            Yii::t('app', 'This Month') => ["moment().startOf('month')", "moment().endOf('month')"],
                            Yii::t('app', 'Last Month') => [
                                "moment().subtract(1, 'month').startOf('month')",
                                "moment().subtract(1, 'month').endOf('month')",
                            ],
                            Yii::t('app', 'Next Month') => [
                                "moment().add(1, 'month').startOf('month')",
                                "moment().add(1, 'month').endOf('month')",
                            ],
                        ],
                    ],
                ],
                
                # EDITABLE SETTINGS
                'editableOptions' => [
                    'header' => 'Expected Billing Date',
                    'size' => 'md',
                    'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                    'formOptions' => ['action' => ['job/pjax-edit']], // controller action to dispatch the updates to
                    'inputType' => \kartik\editable\Editable::INPUT_WIDGET,
                    'widgetClass' => 'kartik\datecontrol\DateControl',
                    'options' => [
                        # see: http://demos.krajee.com/datecontrol#defaults
                        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                        // 'displayFormat' => 'dd-MM-yyyy',
                        // 'saveFormat' => 'php:Y-m-d',
                        'options' => [
                            // 'convertFormat' => true, // autoconvert PHP format to JS format
                            'pluginOptions' => [
                                'autoclose' => true,
                                // 'format' => 'php:Y-m-d' // php date format
                            ]
                        ]
                    ]
                ],
            ],
            // 'date_created',
            // 'date_modified',
            [
                'class' => 'kartik\grid\ActionColumn',
                'vAlign' => 'middle',
                'viewOptions' => [/*'title' => 'View Job details', 'data-toggle' => 'tooltip'*/],
                'updateOptions' => [/*'title' => 'Edit Job details', 'data-toggle' => 'tooltip'*/],
                'deleteOptions' => [/*'title' => 'Delete this Job?', 'data-toggle' => 'tooltip'*/],
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'visibleButtons' => [
                    'delete' => \Yii::$app->user->can('deleteJob'),
                    'update' => \Yii::$app->user->can('updateJob'),
                ],
            ],
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'kartik-sheet-style'],
            ],
        ],
        // </editor-fold>
    ]);
    ?>

</div>
