<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $job app\models\Job */

$this->title = 'Update Job: ' . $job->job_id;
$this->params['breadcrumbs'][] = ['label' => 'Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $job->job_id, 'url' => ['view', 'id' => $job->job_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="job-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'job' => $job,
    ]) ?>

</div>
