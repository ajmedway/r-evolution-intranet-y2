<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\Client;
use app\models\Person;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if (\Yii::$app->user->can('createClient')): ?>
        <p>
            <?= Html::a('Create Client', ['create'], ['class' => 'btn btn-revs']) ?>
        </p>
    <?php endif; ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'striped' => true,
        'hover' => false,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'vAlign' => 'middle',
            ],
            [
                'attribute' => 'client_id',
                'vAlign' => 'middle',
            ],
            [
                'attribute' => 'hs_company_id',
                'vAlign' => 'middle',
            ],
            [
                // Client name (with editable popover)
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'name',
                'vAlign' => 'middle',
                'width' => '220px',
                'editableOptions' => [
                    'size' => 'md',
                    // controller action to dispatch the updates to
                    'formOptions' => ['action' => ['client/pjax-edit']],
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                ],
            ],
            [
                // Relationship Manager name with person dropdown (with editable popover)
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'relationship_manager_id',
                'label' => 'Relationship Manager',
                'vAlign' => 'middle',
                'width' => '250px',
                'value' => 'relationshipManager.name',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Person::find()->orderBy('name')->asArray()->all(), 'person_id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Any Client'],
                'editableOptions' => [
                    'header' => 'Client',
                    'size' => 'md',
                    'placement' => \kartik\popover\PopoverX::ALIGN_RIGHT,
                    'formOptions' => ['action' => ['client/pjax-edit']], // controller action to dispatch the updates to
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => ArrayHelper::map(Person::find()->orderBy('name')->asArray()->all(), 'person_id', 'name'),
                ],
            ],
            [
                'attribute' => 'date_created',
                'vAlign' => 'middle',
                'format' => 'date',
            ],
            [
                'attribute' => 'date_modified',
                'vAlign' => 'middle',
                'format' => 'date',
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'vAlign' => 'middle',
                'viewOptions' => ['title' => 'View Job details', 'data-toggle' => 'tooltip'],
                'updateOptions' => ['title' => 'Edit Job details', 'data-toggle' => 'tooltip'],
                'deleteOptions' => ['title' => 'Delete this Job?', 'data-toggle' => 'tooltip'],
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'visibleButtons' => [
                    'delete' => \Yii::$app->user->can('deleteClient'),
                    'update' => \Yii::$app->user->can('updateClient'),
                ],
            ],
        ],
    ]);
    ?>

</div>
