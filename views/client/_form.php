<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Person;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */

// get data for populating the form/dropdown options etc.
$people = Person::find()->peopleTeamsOptions()->asArray()->all();
$peopleOptions = ArrayHelper::map($people, 'person_id', 'name_team', 'type');
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row hidden">
            <div class="col-sm-6">
                <?= $form->field($model, 'guid')->textInput(['maxlength' => true, 'readonly'=>'readonly', 'disabled'=>'disabled']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'hs_company_id')->textInput(['maxlength' => true, 'readonly'=>'readonly', 'disabled'=>'disabled']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'relationship_manager_id')->dropDownList($peopleOptions, ['prompt' => 'Select Relationship Manager'])->label('Relationship Manager') ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-revs' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
