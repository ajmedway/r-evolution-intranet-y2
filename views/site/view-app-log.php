<?php

/* @var $this yii\web\View */
/* @var $appLog string */

use yii\helpers\Html;

$this->title = 'Application Log';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <code><?= nl2br($appLog) ?></code>
</div>
