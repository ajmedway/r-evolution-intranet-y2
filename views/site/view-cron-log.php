<?php

/* @var $this yii\web\View */
/* @var $cronLog string */

use yii\helpers\Html;

$this->title = 'Cron Log';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <code><?= nl2br($cronLog) ?></code>
</div>
