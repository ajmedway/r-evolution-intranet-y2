<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'r//evolution Intranet';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><span class="revorange">r//evolution</span> Intranet</h1>

        <p class="lead">In-house toolkit for tracking staff, clients, jobs and billings.</p>

        <p><a class="btn btn-lg btn-revs" href="<?= Url::to('dashboard') ?>">View Dashboard &raquo;</a></p>
    </div>

    <div class="body-content">

        <div class="row" id="main-links">
            <div class="col-lg-4">
                <h2>People</h2>

                <p>Members of staff @ <span class="revorange">r//evolution</span>.</p>

                <p><a class="btn btn-default" href="<?= Url::to('person') ?>">Manage People &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Clients</h2>

                <p>Clients of <span class="revorange">r//evolution</span>.</p>

                <p><a class="btn btn-default" href="<?= Url::to('client') ?>">Manage Clients &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Jobs</h2>

                <p>Jobs and projects @ <span class="revorange">r//evolution</span>.</p>

                <p><a class="btn btn-default" href="<?= Url::to('job') ?>">Manage Jobs &raquo;</a></p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <h4>See Also...</h4>
                <ul class="list-unstyled">
                    <li><a class="btn btn-link" href="<?= Url::to('site/view-app-log') ?>">View Application Log</a></li>
                    <li><a class="btn btn-link" href="<?= Url::to('site/view-cron-log') ?>">View Cron Log</a></li>
                </ul>
            </div>
        </div>

    </div>
</div>
