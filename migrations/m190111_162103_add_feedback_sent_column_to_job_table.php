<?php

use yii\db\Migration;

/**
 * Handles adding feedback_sent to table `job`.
 */
class m190111_162103_add_feedback_sent_column_to_job_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('job', 'feedback_sent', $this->boolean()->after('expected_billing_date')->defaultValue(0));
        
        $this->createIndex(
            'idx-feedback_sent',
            'job',
            'feedback_sent'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-feedback_sent',
            'job'
        );
        
        $this->dropColumn('job', 'feedback_sent');
    }
}
