<?php

use yii\db\Migration;

/**
 * Class m180406_090435_initial_schema
 * 
 * @see https://www.yiiframework.com/doc/guide/2.0/en/db-migrations
 */
class m180406_090435_initial_schema extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";

        /* MYSQL */
        if (!in_array('client', $tables)) {
            if ($dbType == "mysql") {
                $this->createTable('{{%client}}', [
                    'client_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`client_id`)',
                    'guid' => 'CHAR(32) NULL',
                    'relationship_manager_id' => 'INT(11) NOT NULL',
                    'name' => 'VARCHAR(255) NOT NULL',
                    'date_created' => 'DATETIME NULL',
                    'date_modified' => 'DATETIME NULL',
                    ], $tableOptions_mysql);
            }
        }

        /* MYSQL */
        if (!in_array('job', $tables)) {
            if ($dbType == "mysql") {
                $this->createTable('{{%job}}', [
                    'job_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`job_id`)',
                    'guid' => 'CHAR(32) NULL',
                    'client_id' => 'INT(11) NOT NULL',
                    'project_manager_id' => 'INT(11) NOT NULL',
                    'job_name' => 'VARCHAR(255) NOT NULL',
                    'quote_rate' => 'FLOAT NOT NULL',
                    'quote_unit' => 'VARCHAR(255) NOT NULL',
                    'total_units' => 'FLOAT NOT NULL',
                    'total_value' => 'FLOAT NOT NULL',
                    'sale_total_value' => 'FLOAT NOT NULL',
                    'in_cost' => 'FLOAT NOT NULL',
                    'margin' => 'FLOAT NOT NULL',
                    'state' => 'VARCHAR(255) NOT NULL',
                    'expected_billing_date' => 'DATE NOT NULL',
                    'date_created' => 'DATETIME NULL',
                    'date_modified' => 'DATETIME NULL',
                    ], $tableOptions_mysql);
            }
        }

        /* MYSQL */
        if (!in_array('person', $tables)) {
            if ($dbType == "mysql") {
                $this->createTable('{{%person}}', [
                    'person_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`person_id`)',
                    'guid' => 'CHAR(32) NULL',
                    'name' => 'VARCHAR(255) NOT NULL',
                    'team' => 'VARCHAR(255) NOT NULL',
                    'active' => 'TINYINT(1) NOT NULL',
                    'date_created' => 'DATETIME NULL',
                    'date_modified' => 'DATETIME NULL',
                    ], $tableOptions_mysql);
            }
        }

        /* MYSQL */
        if (!in_array('person_on_job', $tables)) {
            if ($dbType == "mysql") {
                $this->createTable('{{%person_on_job}}', [
                    'person_on_job_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`person_on_job_id`)',
                    'job_id' => 'INT(11) NOT NULL',
                    'person_id' => 'INT(11) NOT NULL',
                    'time_units' => 'FLOAT NOT NULL',
                    'date_created' => 'DATETIME NULL',
                    'date_modified' => 'DATETIME NULL',
                    ], $tableOptions_mysql);
            }
        }

        /* MYSQL */
        if (!in_array('record', $tables)) {
            if ($dbType == "mysql") {
                $this->createTable('{{%record}}', [
                    'record_id' => 'INT(11) NOT NULL',
                    0 => 'PRIMARY KEY (`record_id`)',
                    'attr1' => 'INT(11) NOT NULL',
                    'attr2' => 'INT(11) NOT NULL',
                    ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_UNIQUE_name_1515_00', 'client', 'name', 1);
        $this->createIndex('idx_relationship_manager_id_1515_01', 'client', 'relationship_manager_id', 0);
        $this->createIndex('idx_client_id_1541_02', 'job', 'client_id', 0);
        $this->createIndex('idx_project_manager_id_1542_03', 'job', 'project_manager_id', 0);
        $this->createIndex('idx_job_id_1587_04', 'person_on_job', 'job_id', 0);
        $this->createIndex('idx_person_id_1587_05', 'person_on_job', 'person_id', 0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_person_1511_00', '{{%client}}', 'relationship_manager_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_client_1537_01', '{{%job}}', 'client_id', '{{%client}}', 'client_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_person_1537_02', '{{%job}}', 'project_manager_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_job_1584_03', '{{%person_on_job}}', 'job_id', '{{%job}}', 'job_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_person_1584_04', '{{%person_on_job}}', 'person_id', '{{%person}}', 'person_id', 'CASCADE', 'CASCADE');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180406_090435_initial_schema cannot be reverted.\n";
        
//        $this->execute('SET foreign_key_checks = 0');
//        $this->execute('DROP TABLE IF EXISTS `client`');
//        $this->execute('SET foreign_key_checks = 1;');
//        $this->execute('SET foreign_key_checks = 0');
//        $this->execute('DROP TABLE IF EXISTS `job`');
//        $this->execute('SET foreign_key_checks = 1;');
//        $this->execute('SET foreign_key_checks = 0');
//        $this->execute('DROP TABLE IF EXISTS `person`');
//        $this->execute('SET foreign_key_checks = 1;');
//        $this->execute('SET foreign_key_checks = 0');
//        $this->execute('DROP TABLE IF EXISTS `person_on_job`');
//        $this->execute('SET foreign_key_checks = 1;');
//        $this->execute('SET foreign_key_checks = 0');
//        $this->execute('DROP TABLE IF EXISTS `record`');
//        $this->execute('SET foreign_key_checks = 1;');

        return false;
    }

    // Use up()/down() to run migration code without a transaction.
//    public function up()
//    {
//
//    }
//
//    public function down()
//    {
//        echo "m180406_090435_initial_schema cannot be reverted.\n";
//
//        return false;
//    }
}
