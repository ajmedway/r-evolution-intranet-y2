<?php

use yii\db\Migration;

/**
 * Handles adding hs_deal_id to table `job`.
 */
class m190104_132538_add_hs_deal_id_column_to_job_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('job', 'hs_deal_id', $this->integer()->defaultValue(NULL)->after('guid'));
        
        $this->createIndex(
            'idx-hs_deal_id',
            'job',
            'hs_deal_id',
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-hs_deal_id',
            'job'
        );
        
        $this->dropColumn('job', 'hs_deal_id');
    }
}
