<?php

use yii\db\Migration;

/**
 * Handles adding created_by_id to table `job`.
 * Has foreign keys to the tables:
 *
 * - `person`
 * 
 * @see https://www.yiiframework.com/doc/guide/2.0/en/db-migrations
 * 
 * Command used: ./yii migrate/create add_created_by_id_column_to_job_table --fields="created_by_id:integer:notNull:defaultValue(12):after('project_manager_id'):foreignKey(person)"
 */
class m180406_102724_add_created_by_id_column_to_job_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn(
            'job',
            'created_by_id',
            $this->integer()->notNull()->defaultValue(12)->after('project_manager_id')
        );

        // creates index for column `created_by_id`
        $this->createIndex(
            'idx-job-created_by_id',
            'job',
            'created_by_id'
        );

        // add foreign key for table `person`
        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey(
            'fk-job-created_by_id',
            'job',
            'created_by_id',
            'person',
            'person_id',
            'NO ACTION'
        );
        $this->execute('SET foreign_key_checks = 1;');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        // drops foreign key for table `person`
        $this->execute('SET foreign_key_checks = 0');
        $this->dropForeignKey(
            'fk-job-created_by_id',
            'job'
        );
        $this->execute('SET foreign_key_checks = 1;');

        // drops index for column `created_by_id`
        $this->dropIndex(
            'idx-job-created_by_id',
            'job'
        );

        $this->dropColumn('job', 'created_by_id');
    }
}
