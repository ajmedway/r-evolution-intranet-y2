<?php

use yii\db\Migration;

/**
 * Handles adding team_lead to table `person`.
 */
class m190104_132557_add_team_lead_column_to_person_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('person', 'team_lead', $this->boolean()->defaultValue(0)->after('team'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('person', 'team_lead');
    }
}
