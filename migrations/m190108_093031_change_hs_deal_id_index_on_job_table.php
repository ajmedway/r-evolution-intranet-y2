<?php

use yii\db\Migration;

/**
 * Class m190108_093031_change_hs_deal_id_index_on_job_table
 */
class m190108_093031_change_hs_deal_id_index_on_job_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->dropIndex(
            'idx-hs_deal_id',
            'job'
        );
        
        $this->createIndex(
            'idx-hs_deal_id',
            'job',
            'hs_deal_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        echo "m190108_093031_change_hs_deal_id_index_on_job_table cannot be reverted.\n";

        return false;
    }
}
