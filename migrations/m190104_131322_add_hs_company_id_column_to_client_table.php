<?php

use yii\db\Migration;

/**
 * Handles adding hs_company_id to table `client`.
 */
class m190104_131322_add_hs_company_id_column_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'hs_company_id', $this->integer()->defaultValue(NULL)->after('guid'));
        
        $this->createIndex(
            'idx-hs_company_id',
            'client',
            'hs_company_id',
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-hs_company_id',
            'client'
        );
        
        $this->dropColumn('client', 'hs_company_id');
    }
}
