<?php
use kartik\datecontrol\Module as datecontrol;

/**
 * See - https://github.com/kartik-v/yii2-datecontrol
 *     - http://demos.krajee.com/datecontrol
 */

return [
    'adminEmail' => 'admin@example.com',
    
    // format settings for displaying each date attribute (ICU format example)
    'dateControlDisplay' => [
        datecontrol::FORMAT_DATE => 'dd-MM-yyyy',
        datecontrol::FORMAT_TIME => 'hh:mm:ss a',
        datecontrol::FORMAT_DATETIME => 'dd-MM-yyyy hh:mm:ss a',
    ],
    
    // format settings for saving each date attribute (PHP format example)
    'dateControlSave' => [
        // datecontrol::FORMAT_DATE => 'php:U', // saves as unix timestamp
        datecontrol::FORMAT_DATE => 'php:Y-m-d',
        datecontrol::FORMAT_TIME => 'php:H:i:s',
        datecontrol::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
    ],
    
    // set your display timezone
    'displayTimezone' => 'Europe/London',

    // set your timezone for date saved to db
    'saveTimezone' => 'UTC',

    // automatically use kartik\widgets for each of the above formats
    'autoWidget' => true,

    // use ajax conversion for processing dates from display format to save format.
    'ajaxConversion' => true,

    // default settings for each widget from kartik\widgets used when autoWidget is true
    'autoWidgetSettings' => [
        datecontrol::FORMAT_DATE => ['type' => 2, 'pluginOptions' => ['autoclose' => true]], // example
        datecontrol::FORMAT_DATETIME => [], // setup if needed
        datecontrol::FORMAT_TIME => [], // setup if needed
    ],
    
    // custom widget settings that will be used to render the date input instead of kartik\widgets,
    // this will be used when autoWidget is set to false at module or widget level.
    'widgetSettings' => [
        datecontrol::FORMAT_DATE => [
            'class' => 'yii\jui\DatePicker', // example
            'options' => [
                'dateFormat' => 'php:Y-m-d',
                'options' => ['class' => 'form-control'],
            ]
        ]
    ]
];
