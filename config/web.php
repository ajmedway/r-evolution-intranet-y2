<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'revolution-intranet',
    'name' => 'r//evolution Intranet',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'maintenanceMode'],
    'timeZone' => 'Europe/London',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'defaultRoute' => 'site/index',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '6f7t5JcRIZ8i3KdvjKZOuSSb2YotzyNu',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            //'defaultRoles' => ['user'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 6 : 0,
            // see: https://www.yiiframework.com/doc/api/2.0/yii-log-target
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'welcome' => 'site/login',
                'dashboard' => 'job/dashboard',
            ],
        ],
        'formatter' => [
            // http://userguide.icu-project.org/formatparse
            'dateFormat' => 'dd-MM-yyyy',
            'datetimeFormat' => 'medium',
            'decimalSeparator' => '.',
            'thousandSeparator' => ',',
            'currencyCode' => 'GBP',
        ],
        'hubspotDataManager' => [
            'class' => app\components\managers\HubspotDataManager::class,
            // https://app.hubspot.com/integrations-settings/3021617/api-key
            'hubspotApiKey' => '21cc3800-0c20-4c57-b3bb-abd6afb3dae7',
        ],
        'maintenanceMode' => [
            'class' => 'brussens\maintenance\MaintenanceMode',

            // Mode status
            'enabled' => false,

            // Route to action
            'route' => 'maintenance/index',

            // Show title
            'title' => 'Site under maintenance...',

            // Show message
            'message' => 'Sorry, this site is currently undergoing some maintenance work. Please try again later.',

            // Allowed user names
            'users' => [
                'admin',
                'alec@r-evolution.co.uk',
                'sam@r-evolution.co.uk',
            ],

            // Allowed roles
            'roles' => [],

            // Allowed IP addresses
            'ips' => [],

            // Allowed URLs
            'urls' => ['welcome', 'site/login', 'site/logout'],

            // Layout path
            'layoutPath' => '@app/views/layouts/main.php',

            // View path
            'viewPath' => '@app/views/maintenance/view.php',

            // User name attribute name
            'usernameAttribute' => 'username',

            // HTTP Status Code
            'statusCode' => 503,

            // Retry-After header
            'retryAfter' => 120, // or Wed, 21 Oct 2015 07:28:00 GMT for example
        ],
    ],
    // Global access control - redirect to holding page if not hitting the login/welcome url
    'as beforeRequest' => [
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            // allow guests to access the login page
            [
                'allow' => true,
                'actions' => ['login'],
                'roles' => ['?'],
            ],
            // allow authenticated users
            [
                'allow' => true,
                'roles' => ['@'],
            ],
            // everything else is denied
        ],
        'denyCallback' => function () {
            return Yii::$app->response->redirect('https://www.r-evolution.co.uk/');
        },
    ],
    'modules' => [
        'gridview' => [
            'class' => 'kartik\grid\Module'
        ],
        'datecontrol' =>  [
            'class' => 'kartik\datecontrol\Module',
        ],
        'migration-utility' => [
            'class' => 'c006\utility\migration\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
        'allowedIPs' => ['127.0.0.1', '::1', '185.59.181.136'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
        'allowedIPs' => ['127.0.0.1', '::1', '185.59.181.136'],
    ];
}

return $config;
