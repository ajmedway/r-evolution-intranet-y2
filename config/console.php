<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'Europe/London',
    'controllerNamespace' => 'app\commands',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            //'defaultRoles' => ['user'],
        ],
        'log' => [
            // see: https://www.yiiframework.com/doc/api/2.0/yii-log-target
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/cron.log',
                    'levels' => ['error', 'warning', 'info'],
                    'logVars' => ['_GET'],
                ],
            ],
        ],
        'db' => $db,
        'hubspotDataManager' => [
            'class' => app\components\managers\HubspotDataManager::class,
            // https://app.hubspot.com/integrations-settings/3021617/api-key
            'hubspotApiKey' => '21cc3800-0c20-4c57-b3bb-abd6afb3dae7',
        ],
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
