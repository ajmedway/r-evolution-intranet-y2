<?php

// hinge the debug/environment variables
$yiiDbg = (1 && isset($_SERVER['HTTP_ALEC'])) ? true : false;
$yiiEnv = (1 && isset($_SERVER['HTTP_ALEC'])) ? 'dev' : 'production';

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', $yiiDbg);
defined('YII_ENV') or define('YII_ENV', $yiiEnv);

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();
