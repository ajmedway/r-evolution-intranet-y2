var $ = jQuery.noConflict();

$(document).ready(function ($) {
    
    // Bootstrap Tooltip and Popover initialisers
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
    $(function () {
        $("[data-toggle='popover']").popover();
    });
    
    // Handler function to filter out non-numeric float keys on keydown
    $(".numericFloatKeysOnly").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
    if ($('.job-form').length) {
        $(document).on('keyup change', function (event) {
            // calculate totals on each change and keystroke
            var totalUnits = 0,
                matches = false,
                $jobTotalUnits = $('#job-total_units'),
                $jobQuoteRate = $('#job-quote_rate'),
                $jobTotalValue = $('#job-total_value'),
                $jobSaleTotalValue = $('#job-sale_total_value'),
                $jobInCost = $('#job-in_cost'),
                $jobMargin = $('#job-margin');
            
            // check if the calculated and sale total fields match
            if ($jobSaleTotalValue.val() === $jobTotalValue.val()) {
                matches = true;
            }
            
            $('.person-on-job-rows').find('.time_units').each(function(i) {
                var personTimeUnits = $(this).val() ? $(this).val() : 0;
                totalUnits += parseFloat(personTimeUnits);
            });
            $jobTotalUnits.val(totalUnits);
            
            var total = $jobQuoteRate.val() * $jobTotalUnits.val();
            $jobTotalValue.val(total);
            
            // if the calculated and sale total fields match, keep them in sync (defaulting purposes)
            if (matches) {
                $jobSaleTotalValue.val(total);
            }
            
            var margin = $jobSaleTotalValue.val() - $jobInCost.val();
            $jobMargin.val(margin);
        });
    }
    
    $(document).on('change', '#job-quote_unit', function () {
        // Enforce default values on change of quote unit unless a non-default value has been set
        var unit = $(this).val();
        var rate = parseInt($('#job-quote_rate').val());
        if (rate === 0 || rate === 100 || rate === 600) {
            if (unit === 'Hour') {
                rate = 100;
            } else if (unit === 'Day') {
                rate = 600;
            } else if ($.inArray(unit, ['', 'Fixed'])) {
                rate = 0;
            }
            $('#job-quote_rate').val(rate);
        }
        
        // Handle form update for changes to/from Fixed mode
        if (unit === '') {
            $('select.person_id optgroup[label="People"]').prop('disabled', true);
            $('select.person_id optgroup[label="Teams"]').prop('disabled', true);
        } else if (unit === 'Fixed') {
            $('select.person_id optgroup[label="People"]').prop('disabled', true);
            $('select.person_id optgroup[label="Teams"]').prop('disabled', false);
        } else {
            $('select.person_id optgroup[label="People"]').prop('disabled', false);
            $('select.person_id optgroup[label="Teams"]').prop('disabled', true);
        }
    });
    
    $(document).on('change', 'select.person_id', function () {
        // Find current people on job selection types
        var hasNoSelection = false;
        var hasTeamSelection = false;
        var hasPersonSelection = false;
        $('select.person_id').each(function(i) {
            var selectedText = $(this).find('option:selected').text();
            if (selectedText.indexOf('Team member') >= 0) {
                hasNoSelection = true;
            } else if (selectedText.indexOf('Team') >= 0) {
                hasTeamSelection = true;
            } else if (selectedText.length > 0) {
                hasPersonSelection = true;
            }
        });
        
        // Handle form update for changes to/from Fixed mode
        if (hasNoSelection && $('select.person_id').length >= 1) {
            $('#job-quote_unit option[value="Hour"]').prop('disabled', false);
            $('#job-quote_unit option[value="Day"]').prop('disabled', false);
            $('#job-quote_unit option[value="Fixed"]').prop('disabled', false);
        } else if (hasTeamSelection) {
            $('#job-quote_unit option[value="Hour"]').prop('disabled', true);
            $('#job-quote_unit option[value="Day"]').prop('disabled', true);
            $('#job-quote_unit option[value="Fixed"]').prop('disabled', false);
        } else if (hasPersonSelection) {
            $('#job-quote_unit option[value="Hour"]').prop('disabled', false);
            $('#job-quote_unit option[value="Day"]').prop('disabled', false);
            $('#job-quote_unit option[value="Fixed"]').prop('disabled', true);
        }
    });

    $(document).on('click', '.add-person', function () {
        // get the last person on job div 
        var $div = $('div[id^="person_on_job_"]:last');
        
        // Read the Number from the ID and increment by 1
        var oldnum = parseInt($div.prop('id').match(/\d+/g), 10);
        var num = parseInt($div.prop('id').match(/\d+/g), 10) + 1;
        
        // Clone it and assign the new ID
        var $cloned_div = $div.clone().prop('id', 'person_on_job_' + num);
        
        // remove the person_on_job_id for cloned rows
        $cloned_div.find('.wrapper-person_on_job_id').remove();
        
        // update id's
        $cloned_div.find('.person_id')
            .attr('id', 'persononjob-' + num + '-person_id')
            .attr('name', 'PersonOnJob[' + num + '][person_id]')
            .val('');
        
        $cloned_div.find('.time_units')
            .attr('id', 'persononjob-' + num + '-time_units')
            .attr('name', 'PersonOnJob[' + num + '][time_units]')
            .val(0);
        
        $cloned_div.appendTo('.person-on-job-rows');
        
        $('#person_on_job_' + num + ' a').removeAttr('href')
            .removeAttr('data-confirm')
            .removeAttr('data-method')
            .removeAttr('data-pjax')
            .addClass('remove-person');
    });

    $(document).on('click', '.remove-person', function () {
        var parent_id = $(this).parents('.row.person_on_job').prop('id');
        // delete row if person removed
        $(this).parents('.row.person_on_job').remove();
    });
    
    // Initialise disabled people lists based on quote units selection
    $('#job-quote_unit').change();
    
    // Initialise disabled quote units selection based on current people lists
    $('select.person_id').change();
    
//    // use krajeeDialog object instance initialized by the widget
//    $('#job-quote_unit').on('change', function () {
//        krajeeDialog.alert('An alert');
//        // or show a confirm
//        krajeeDialog.confirm('Are you sure', function (out) {
//            if (out) {
//                alert('Yes'); // or do something on confirmation
//            }
//        });
//    });
//
//    // use krajeeDialogCust object instance
//    $('#btn-2').on('click', function () {
//        krajeeDialogCust.alert('An alert');
//        // or show a prompt
//        krajeeDialogCust.prompt({label: 'Provide reason', placeholder: 'Upto 30 characters...'}, function (out) {
//            if (out) {
//                alert('Yes'); // or do something based on the value of out
//            }
//        });
//    });

    var pjaxContainers = [
        '#pjax-grid-Communications',
        '#pjax-grid-Design',
        '#pjax-grid-Digital',
        '#pjax-grid-OLM',
        '#pjax-grid-ProjectManagement',
        '#pjax-grid-Other',
    ];
    
    $('#pjax-grid-dashboardMain').on('pjax:success', function() {
        
        console.log('#pjax-grid-dashboardMain pjax:success');
        
        $(this).one('pjax:end', function() {
            
            console.log('#pjax-grid-dashboardMain pjax:end');
            
            // trigger sequential data reloads of breakdown gridviews
            $.each(pjaxContainers, function (index, container) {
                if (index + 1 < pjaxContainers.length) {
                    // when pjax container has completed reload, reload the next one
                    $(container).one('pjax:end', function (xhr, options) {
                        $.pjax.reload({container: pjaxContainers[index + 1], timeout: false}).done(function() {
                            console.log(pjaxContainers[index + 1] + ' data refreshed.');
                        });
                    });
                }
            });
            
            // kick-off subsequent requests
            $.pjax.reload({container: pjaxContainers[0]}).done(function() {
                console.log('INITIAL: ' + pjaxContainers[0] + ' data refreshed.');
            });
            
        });
        
    });
    
    function floatingHeaderHide() {
        $('.kv-thead-float').css('opacity', 0);
    }
    
    function floatingHeaderReflow() {
        // update position of floating header
        $('#pjax-grid-dashboardMain table').trigger('reflow');
        
        // wait 400ms then fade back in in correct position
        setTimeout(function() {
            $('.kv-thead-float').fadeTo(200, 1);
        }, 400);
    }
    
    // toggle show/hide team breakdown details
    $('.job-index').on('click', '#accordion-teamBreakdowns .btn-details-toggle', function() {
        $(this).closest('.panel > .breakdown-data').find('.table.breakdown-data').toggleClass('no-details');
        floatingHeaderHide();
        floatingHeaderReflow();
    });
    
    // when one of the panels above the dashboard is expanded or collapsed, set the floating header opacity to zero
    $('.job-index').on('hide.bs.collapse show.bs.collapse', '#accordion-teamBreakdowns .collapse', function () {
        floatingHeaderHide();
    });
    
    // trigger position reflow on main dashboard floating header, then fade back into view
    $('.job-index').on('hidden.bs.collapse shown.bs.collapse', '#accordion-teamBreakdowns .collapse', function () {
        floatingHeaderReflow();
    });
    
});