<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use kartik\grid\EditableColumnAction;
use app\models\Client;
use app\models\Job;
use app\models\JobSearch;
use app\models\Person;
use app\models\PersonOnJob;

/**
 * JobController implements the CRUD actions for Job model.
 */
class JobController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Editable Pjax/Ajax Endpoints
     * See - http://webtips.krajee.com/rapidly-setup-gridview-editable-cells-with-editable-column-action/
     *     - http://webtips.krajee.com/setup-editable-column-grid-view-manipulate-records/
     *     - http://demos.krajee.com/grid#editable-column
     * 
     * @return mixed
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
                // identifier for your editable action
                'pjax-edit' => [
                    // action class name
                    'class' => EditableColumnAction::className(),
                    // the update model class
                    'modelClass' => Job::className(),
                    'outputValue' => function ($model, $attribute, $key, $index) {
                        $fmt = Yii::$app->formatter;

                        // your attribute value
                        $value = $model->$attribute;

                        // return custom formatted values to the form for specific attributes
                        if ($attribute === 'expected_billing_date') {
                            /**
                             * See: https://github.com/kartik-v/yii2-grid/issues/166#issuecomment-66989120
                             * Kartik recommends converting the output value manually on ajax response for dates/times
                             * where attribute/model format differs from display format
                             */
                            return $fmt->asDate($value, 'dd-MM-yyyy');
                        } else if ($attribute === 'client_id') {
                            return $model->client->name;
                        } else if ($attribute === 'project_manager_id') {
                            return $model->projectManager->name;
                        } else if ($attribute === 'feedback_sent') {
                            return $model->feedback_sent ? 'Yes' : 'No';
                        }

                        // empty is same as $value
                        return '';
                    },
                ]
        ]);
    }

    /**
     * Monthly Dashboard
     * @return mixed
     */
    public function actionDashboard()
    {
        $searchModel = new JobSearch();
        $expectedBillingDateFilterFormat = 'd-m-Y';
        
        $searchModel->setExpectedBillingDateFilterDefaults($expectedBillingDateFilterFormat);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $expectedBillingDateFilterFormat);
        
        // initialise render params
        $renderParams = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
        
        foreach (Person::teams() as $teamName) {
            $camelisedTeamName = \yii\helpers\Inflector::camelize($teamName);
            $renderParams["dataProvider{$camelisedTeamName}Breakdown"] = $searchModel->teamBreakdown(Yii::$app->request->queryParams, $teamName, $expectedBillingDateFilterFormat);
        }
        
        // set full-width display mode
        $this->view->params['bsContainerClass'] = 'container-fluid';

        return $this->render('dashboard', $renderParams);
    }

    /**
     * Lists all Job models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Job model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Job model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $post = $request->post();

        $job = new Job();
        $job->loadDefaultValues();
        
        if ($job->load($post) && $job->save()) {

            // create fresh models for each person on job submitted
            $count = count(Yii::$app->request->post('PersonOnJob', []));
            for ($i = 1; $i <= $count; $i++) {
                $peopleOnJob[] = new PersonOnJob();
            }

            // hydrate PersonOnJob models from post
            if (ActiveRecord::loadMultiple($peopleOnJob, $post)) {
                foreach ($peopleOnJob as $personOnJob) {
                    $personOnJob->job_id = $job->job_id;
                    $personOnJob->save();
                }
            }

            return $this->redirect(['job/dashboard']);
            // return $this->redirect(['view', 'id' => $job->job_id]);
        } else {
            return $this->render('create', [
                    'job' => $job,
            ]);
        }
    }

    /**
     * Updates an existing Job model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        
        // get referrer data to handle redirection to dashboard on successful update
        $referrer = new \yii\web\Request(['url' => parse_url($request->referrer, PHP_URL_PATH)]);
        $referrerRoute = rtrim(Yii::$app->urlManager->parseRequest($referrer)[0], '/');
        if ($referrerRoute === 'job/dashboard') {
            $session->setFlash('dashboardRedirectUrl', $request->referrer);
        }
        
        $post = $request->post();
        
        $job = $this->findModel($id);
        
        if ($job->load($post) && $job->save()) {
            /*
             * 1. Analyse current people on job
             * 2. Handle updates via person_on_job_id / creation of new records
             * 3. Handle removals (i.e. missing id's)
             */
            $personOnJobIds = ArrayHelper::getColumn($job->personOnJobs, 'person_on_job_id');
            $personOnJobIds = array_combine($personOnJobIds, $personOnJobIds);
            
            foreach ($post['PersonOnJob'] as $key => $submittedPersonOnJob) {
                
                // check for presence of person_on_job_id
                $personOnJobId = !empty($submittedPersonOnJob['person_on_job_id']) ? $submittedPersonOnJob['person_on_job_id'] : null;
                
                if ($personOnJobId) {
                    // handle updated person on job record
                    $personOnJob = PersonOnJob::findOne($personOnJobId);
                } else {
                    // create new person on job record
                    $personOnJob = new PersonOnJob();
                }
                
                // load submitted person data and save
                if ($personOnJob) {
                    $personOnJob->load($submittedPersonOnJob, '');
                    $personOnJob->job_id = $job->job_id;
                    
                    if ($personOnJob->save()) {
                        // get the pk and remove from the array of 'old' ids
                        ArrayHelper::remove($personOnJobIds, $personOnJob->getPrimaryKey());
                    }
                }
            }
            
            // clean up the old records that were removed on the form
            PersonOnJob::deleteAll(['job_id' => $job->job_id, 'person_on_job_id' => $personOnJobIds]);
            
            // if we have a dashboard redirect flash session, go there!
            $dashboardRedirectUrl = $session->getFlash('dashboardRedirectUrl');
            if ($dashboardRedirectUrl) {
                return $this->redirect($dashboardRedirectUrl);
            }

            return $this->redirect(['job/dashboard']);
            // return $this->redirect(['view', 'id' => $job->job_id]);
        } else {
            return $this->render('update', [
                    'job' => $job,
            ]);
        }
    }

    /**
     * Deletes an existing Job model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        // get referrer data to handle redirection to dashboard on successful update
        $request = Yii::$app->request;
        $referrer = new \yii\web\Request(['url' => parse_url($request->referrer, PHP_URL_PATH)]);
        $referrerRoute = rtrim(Yii::$app->urlManager->parseRequest($referrer)[0], '/');
        if ($referrerRoute === 'job/dashboard') {
            return $this->redirect($request->referrer);
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Job model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Job the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Job::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
