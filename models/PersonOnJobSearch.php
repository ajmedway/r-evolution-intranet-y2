<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PersonOnJob;

/**
 * PersonOnJobSearch represents the model behind the search form of `app\models\PersonOnJob`.
 */
class PersonOnJobSearch extends PersonOnJob
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['person_on_job_id', 'job_id', 'person_id'], 'integer'],
            [['time_units'], 'number'],
            [['date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PersonOnJob::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'person_on_job_id' => $this->person_on_job_id,
            'job_id' => $this->job_id,
            'person_id' => $this->person_id,
            'time_units' => $this->time_units,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
        ]);

        return $dataProvider;
    }
}
