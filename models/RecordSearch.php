<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Record;

/**
 * RecordSearch represents the model behind the search form of `app\models\Record`.
 */
class RecordSearch extends Record
{
   
    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['sum']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'attr1', 'attr2', 'sum'], 'integer'],
            [['sum'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // find records, additionally selecting the sum of the 2 fields as 'sum'
        $query = Record::find()->select('*, (`attr1` + `attr2`) AS `sum`');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        // enable sorting for the related columns
        $dataProvider->sort->attributes['sum'] = [
            'asc' => ['sum' => SORT_ASC],
            'desc' => ['sum' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'record_id' => $this->record_id,
            'attr1' => $this->attr1,
            'attr2' => $this->attr2,
        ]);
        
            // if the sum has a filter value set, apply the filter in the HAVING clause
            if (is_numeric($this->sum)) {
                $query->having([
                    'sum' => $this->sum,
                ]);
            }

        return $dataProvider;
    }
}
