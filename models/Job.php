<?php

namespace app\models;

use Yii;
use app\components\helpers\Guid;
/**
 * This is the model class for table "job".
 *
 * @property int $job_id
 * @property string $guid
 * @property int $hs_deal_id
 * @property int $client_id
 * @property int $project_manager_id
 * @property int $created_by_id
 * @property string $job_name
 * @property double $quote_rate
 * @property string $quote_unit
 * @property double $total_units
 * @property double $total_value
 * @property double $sale_total_value
 * @property double $in_cost
 * @property double $margin
 * @property string $state
 * @property string $expected_billing_date
 * @property string $feedback_sent
 * @property string $date_created
 * @property string $date_modified
 *
 * @property Client $client
 * @property Person $projectManager
 * @property Person $createdBy
 * @property PersonOnJob[] $personOnJobs
 */
class Job extends \yii\db\ActiveRecord
{
    public static $TIMEUNIT_HOUR = 'Hour';
    public static $TIMEUNIT_DAY = 'Day';
    public static $TIMEUNIT_FIXED = 'Fixed';
    public static $STATE_INVOICED = 'Invoiced';
    public static $STATE_INVOICED_INT = 'Invoiced (Internal)';
    public static $STATE_TO_INVOICE = 'To Invoice';
    public static $STATE_PRE_BILLED = 'Pre-Billed';
    public static $STATE_CONFIRMED = 'Confirmed';
    public static $STATE_PROVISIONAL = 'Provisional';
    public static $STATE_ON_HOLD = 'On Hold';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'job';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'project_manager_id', 'created_by_id', 'job_name', 'quote_rate', 'quote_unit', 'total_units', 'total_value', 'sale_total_value', 'in_cost', 'margin', 'state', 'expected_billing_date'], 'required'],
            [['job_id', 'hs_deal_id', 'client_id', 'project_manager_id', 'created_by_id'], 'integer'],
            [['feedback_sent'], 'boolean'],
            [['job_name', 'quote_unit', 'state'], 'string', 'max' => 255],
            [['quote_rate', 'total_units', 'total_value', 'sale_total_value', 'in_cost', 'margin'], 'number'],
            [['quote_rate', 'total_units', 'total_value', 'sale_total_value', 'in_cost', 'margin'], 'default', 'value' => '0'],
            [['expected_billing_date', 'date_created', 'date_modified'], 'safe'],
            [['guid'], 'string', 'max' => 32],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'client_id']],
            [['project_manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['project_manager_id' => 'person_id']],
            [['created_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['created_by_id' => 'person_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'guid' => 'Guid',
            'hs_deal_id' => 'Hubspot Deal ID',
            'client_id' => 'Client ID',
            'project_manager_id' => 'Project Manager ID',
            'created_by_id' => 'Created By ID',
            'job_name' => 'Job Name',
            'quote_rate' => 'Quote Rate',
            'quote_unit' => 'Quote Unit',
            'total_units' => 'Total Units',
            'total_value' => 'Calculated Total',
            'sale_total_value' => 'Sale Total',
            'in_cost' => 'Buy-In Cost',
            'margin' => 'Margin',
            'state' => 'State',
            'expected_billing_date' => 'Expected Billing Date',
            'feedback_sent' => 'Feedback Sent',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }
    
    /**
     * @param bool return values as keys
     * @return array Time unit strings
     */
    public static function timeUnits($valuesAsKeys = false)
    {
        $data = [
            self::$TIMEUNIT_HOUR,
            self::$TIMEUNIT_DAY,
            self::$TIMEUNIT_FIXED,
        ];
        
        return $valuesAsKeys ? array_combine($data, $data) : $data;
    }
    
    /**
     * @param bool return values as keys
     * @return array Time unit strings
     */
    public static function states($valuesAsKeys = false)
    {
        $data = [
            self::$STATE_INVOICED,
            self::$STATE_INVOICED_INT,
            self::$STATE_TO_INVOICE,
            self::$STATE_PRE_BILLED,
            self::$STATE_CONFIRMED,
            self::$STATE_PROVISIONAL,
            self::$STATE_ON_HOLD,
        ];
        
        return $valuesAsKeys ? array_combine($data, $data) : $data;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectManager()
    {
        return $this->hasOne(Person::className(), ['person_id' => 'project_manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Person::className(), ['person_id' => 'created_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonOnJobs()
    {
        return $this->hasMany(PersonOnJob::className(), ['job_id' => 'job_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['person_id' => 'person_id'])
            ->via('personOnJobs');
    }
    
    /**
     * {@inheritdoc}
     * @return JobQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new JobQuery(get_called_class());
    }

    /**
     * Handle any nuances of data in the model before we commit to insert/update
     * 
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_modified = new \yii\db\Expression('NOW()');
            if (empty($this->date_created)) {
                $this->created_by_id = (Yii::$app instanceof \yii\console\Application) ?
                    \app\models\User::$superAdminID :
                    \Yii::$app->user->getId();
                $this->date_created = new \yii\db\Expression('NOW()');
            }
            if (empty($this->guid)) {
                $this->guid = Guid::generateGuid();
            }
            return true;
        } else {
            return false;
        }
    }

}
