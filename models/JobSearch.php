<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Job;
use app\models\PersonOnJob;
use kartik\daterange\DateRangeBehavior;

/**
 * JobSearch represents the model behind the search form of `app\models\Job`.
 */
class JobSearch extends Job
{
    public $expectedBillingDateRange;
    public $expectedBillingDateStart;
    public $expectedBillingDateEnd;
    
    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['client.name', 'projectManager.name']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'expectedBillingDateRange',
                'dateStartAttribute' => 'expectedBillingDateStart',
                'dateEndAttribute' => 'expectedBillingDateEnd',
            ]
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id', 'hs_deal_id', 'client_id', 'project_manager_id', 'created_by_id'], 'integer'],
            [['feedback_sent'], 'boolean'],
            [['guid', 'job_name', 'quote_unit', 'project_manager_id', 'created_by_id', 'state', 'expected_billing_date', 'date_created', 'date_modified', 'client.name', 'projectManager.name', 'expectedBillingDateStart', 'expectedBillingDateEnd'], 'safe'],
            [['quote_rate', 'total_units', 'total_value', 'sale_total_value', 'in_cost', 'margin'], 'number'],
            [['expectedBillingDateRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Sets default values for the expected billing date filters
     * 
     * @param string $startDate
     * @param string $endDate
     */
    public function setExpectedBillingDateFilterDefaults($format = 'Y-m-d', $startDate = '', $endDate = '')
    {
        // default to first to last dates of current month
        if ($format === 'Y-m-d') {
            $startDate = $startDate ?: date('Y-m-01');
            $endDate = $endDate ?: date('Y-m-t');
        } else if ($format === 'd-m-Y') {
            $startDate = $startDate ?: date('01-m-Y');
            $endDate = $endDate ?: date('t-m-Y');
        }
        
        $this->expectedBillingDateStart = $startDate;
        $this->expectedBillingDateEnd = $endDate;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $expectedBillingDateFilterFormat = 'Y-m-d')
    {
        // get the ActiveQuery instance with relations
        $query = Job::find()->indexBy('job_id')->with('client', 'projectManager', 'createdBy');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // set the default sorting
            'sort' => [
                'enableMultiSort' => true,
                'defaultOrder' => ['client.name' => SORT_ASC, 'expected_billing_date' => SORT_ASC],
            ],
        ]);
        
        // join with relation `projectManager` that is a relation to the table `person`
        // and set the table alias to be `pm`
        $query->joinWith('projectManager AS pm');
        
        // join with relation `createdBy` that is a relation to the table `person`
        // and set the table alias to be `cb`
        $query->joinWith('createdBy AS cb');
        
        // join with relation `client` that is a relation to the table `client`
        // and set the table alias to be `cli`
        $query->joinWith('client AS cli');
        
        // enable sorting for the related columns
        $dataProvider->sort->attributes['client.name'] = [
            'asc' => ['cli.name' => SORT_ASC],
            'desc' => ['cli.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['projectManager.name'] = [
            'asc' => ['pm.name' => SORT_ASC],
            'desc' => ['pm.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['createdBy.name'] = [
            'asc' => ['cb.name' => SORT_ASC],
            'desc' => ['cb.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_id' => $this->job_id,
            'hs_deal_id' => $this->hs_deal_id,
            'cli.client_id' => $this->client_id,
            'pm.person_id' => $this->project_manager_id,
            'cb.person_id' => $this->created_by_id,
            'quote_rate' => $this->quote_rate,
            'total_units' => $this->total_units,
            'total_value' => $this->total_value,
            'sale_total_value' => $this->sale_total_value,
            'in_cost' => $this->in_cost,
            'margin' => $this->margin,
            'feedback_sent' => $this->feedback_sent,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
        ]);

        $query->andFilterWhere(['like', 'guid', $this->guid])
            ->andFilterWhere(['like', 'job_name', $this->job_name])
            ->andFilterWhere(['like', 'quote_unit', $this->quote_unit])
            ->andFilterWhere(['like', 'project_manager_id', $this->project_manager_id])
            ->andFilterWhere(['like', 'created_by_id', $this->created_by_id])
            ->andFilterWhere(['in', 'state', $this->state])
            ->andFilterWhere(['like', 'cli.name', $this->getAttribute('client.name')])
            ->andFilterWhere(['like', 'pm.name', $this->getAttribute('projectManager.name')])
            ->andFilterWhere(['like', 'cb.name', $this->getAttribute('createdBy.name')]);
        
        if (!empty($this->expectedBillingDateStart) && !empty($this->expectedBillingDateEnd)) {
            // filter by date range on expected_billing_date
            $query->andFilterWhere([
                'between',
                'expected_billing_date',
                // hijack the filter dates coming through in another format e.g. 'd-m-Y' and convert ot MySQL format i.e. 'Y-m-d' for the AR query
                \DateTime::createFromFormat($expectedBillingDateFilterFormat, $this->expectedBillingDateStart)->format('Y-m-d'),
                \DateTime::createFromFormat($expectedBillingDateFilterFormat, $this->expectedBillingDateEnd)->format('Y-m-d'),
            ]);
        }
        
        // disable pagination
        $dataProvider->setPagination(false);
        
        return $dataProvider;
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function teamBreakdown($params, $team, $expectedBillingDateFilterFormat = 'Y-m-d')
    {
        // get the ActiveQuery instance with relations
        $query = PersonOnJob::find()->indexBy('person_on_job_id')->with('job', 'person');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // disable sorting
            'sort' => false,
        ]);
        
        // join with relation `job` that is a relation to the table `job`
        // and set the table alias to be `j`, then additionally join onwards to the
        // `client`, `projectManager` and `createdBy` relations of `job`
        $query->joinWith(['job AS j' => function($q) {
            $q->joinWith('client AS cli');
            $q->joinWith('projectManager AS pm');
            $q->joinWith('createdBy AS cb');
        }]);
        
        // join with relation `person` that is a relation to the table `person`
        // and set the table alias to be `p`
        $query->joinWith('person AS p');

        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'j.job_id' => $this->job_id,
            'cli.client_id' => $this->client_id,
            'pm.person_id' => $this->project_manager_id,
            'cb.person_id' => $this->created_by_id,
            'quote_rate' => $this->quote_rate,
            'total_units' => $this->total_units,
            'total_value' => $this->total_value,
            'sale_total_value' => $this->sale_total_value,
            'in_cost' => $this->in_cost,
            'margin' => $this->margin,
            'feedback_sent' => $this->feedback_sent,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
            // add in the team clause
            'p.team' => $team,
        ]);

        $query->andFilterWhere(['like', 'guid', $this->guid])
            ->andFilterWhere(['like', 'job_name', $this->job_name])
            ->andFilterWhere(['like', 'quote_unit', $this->quote_unit])
            ->andFilterWhere(['like', 'project_manager_id', $this->project_manager_id])
            ->andFilterWhere(['like', 'created_by_id', $this->created_by_id])
            ->andFilterWhere(['in', 'j.state', $this->state])
            ->andFilterWhere(['like', 'p.name', $this->getAttribute('person.name')])
            ->andFilterWhere(['like', 'cli.name', $this->getAttribute('client.name')])
            ->andFilterWhere(['like', 'pm.name', $this->getAttribute('projectManager.name')]);
        
        if (!empty($this->expectedBillingDateStart) && !empty($this->expectedBillingDateEnd)) {
            // filter by date range on expected_billing_date
            $query->andFilterWhere([
                'between',
                'expected_billing_date',
                // hijack the filter dates coming through in another format e.g. 'd-m-Y' and convert ot MySQL format i.e. 'Y-m-d' for the AR query
                \DateTime::createFromFormat($expectedBillingDateFilterFormat, $this->expectedBillingDateStart)->format('Y-m-d'),
                \DateTime::createFromFormat($expectedBillingDateFilterFormat, $this->expectedBillingDateEnd)->format('Y-m-d'),
            ]);
        }
        
        // add custom sorting using MySQL `ORDER BY` clause
        $query->orderBy([
            '(CASE WHEN j.state = "' . Job::$STATE_INVOICED . '"
                THEN 9
                   WHEN j.state = "' . Job::$STATE_INVOICED_INT . '"
                THEN 99
                   WHEN j.state = "' . Job::$STATE_TO_INVOICE . '"
                THEN 999
                   WHEN j.state = "' . Job::$STATE_PRE_BILLED . '"
                THEN 9999
                   WHEN j.state = "' . Job::$STATE_CONFIRMED . '"
                THEN 99999
                   WHEN j.state = "' . Job::$STATE_PROVISIONAL . '"
                THEN 999999
                   WHEN j.state = "' . Job::$STATE_ON_HOLD . '"
                THEN 9999999
                ELSE 99999999
              END)' => SORT_ASC,
            'p.name' => SORT_ASC,
        ]);
        
        // disable pagination
        $dataProvider->setPagination(false);
        
        return $dataProvider;
    }
}
