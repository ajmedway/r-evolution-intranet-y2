<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person_on_job".
 *
 * @property int $person_on_job_id
 * @property int $job_id
 * @property int $person_id
 * @property double $time_units
 * @property string $date_created
 * @property string $date_modified
 *
 * @property Job $job
 * @property Person $person
 */
class PersonOnJob extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'person_on_job';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id', 'person_id', 'time_units'], 'required'],
            [['job_id', 'person_id'], 'integer'],
            [['time_units'], 'number'],
            [['date_created', 'date_modified'], 'safe'],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Job::className(), 'targetAttribute' => ['job_id' => 'job_id']],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['person_id' => 'person_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'person_on_job_id' => 'Person On Job ID',
            'job_id' => 'Job ID',
            'person_id' => 'Person ID',
            'time_units' => 'Time Units',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Job::className(), ['job_id' => 'job_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['person_id' => 'person_id']);
    }

    /**
     * {@inheritdoc}
     * @return PersonOnJobQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PersonOnJobQuery(get_called_class());
    }
    
    /**
     * Handle any nuances of data in the model before we commit to insert/update
     * 
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_modified = new \yii\db\Expression('NOW()');
            if ($insert) {
                $this->date_created = new \yii\db\Expression('NOW()');
            }
            return true;
        } else {
            return false;
        }
    }
}
