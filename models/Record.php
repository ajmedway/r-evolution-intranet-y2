<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "record".
 *
 * @property int $record_id
 * @property int $attr1
 * @property int $attr2
 * @property int $sum
 */
class Record extends \yii\db\ActiveRecord
{
    public $sum;
    
    public function getSum()
    {
        $this->sum = 0;
        
        if (is_numeric($this->attr1) && is_numeric($this->attr2)) {
            $this->sum = $this->attr1 + $this->attr2;
        }
        
        return $this->sum;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'record';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'attr1', 'attr2'], 'required'],
            [['record_id', 'attr1', 'attr2', 'sum'], 'integer'],
            [['sum'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'attr1' => 'Attr1',
            'attr2' => 'Attr2',
            'sum' => 'Sum',
        ];
    }

    /**
     * {@inheritdoc}
     * @return RecordQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RecordQuery(get_called_class());
    }
}
