<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Person]].
 *
 * @see Person
 */
class PersonQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * Returns an ActiveQuery object with the Person/Teams data
     * 
     * @return type \yii\db\ActiveQuery
     */
    public function peopleTeamsOptions()
    {
        return $this->addSelect([
                    '*',
                    'IF(name LIKE "%Team", "Teams", "People") AS type',
                    'CONCAT(name, " (", team, ")") AS name_team',
                ])
                ->orderBy([
                    'team' => SORT_ASC,
                    'name' => SORT_ASC,
                ])
                ->indexBy('person_id');
    }

    /**
     * Returns an ActiveQuery object with team/team lead filter
     * 
     * @return type \yii\db\ActiveQuery
     */
    public function teamLead($team)
    {
        return $this->where([
                    'team' => $team,
                    'team_lead' => true,
                ])
                ->orderBy([
                    'name' => SORT_ASC,
                ]);
    }

    /**
     * {@inheritdoc}
     * @return Person[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Person|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
