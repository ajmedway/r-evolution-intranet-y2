<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PersonOnJob]].
 *
 * @see PersonOnJob
 */
class PersonOnJobQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PersonOnJob[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PersonOnJob|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
