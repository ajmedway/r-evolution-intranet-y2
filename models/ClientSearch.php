<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Client;

/**
 * ClientSearch represents the model behind the search form of `app\models\Client`.
 */
class ClientSearch extends Client
{
    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['relationshipManager.name']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'hs_company_id', 'relationship_manager_id'], 'integer'],
            [['guid', 'name', 'relationshipManager.name', 'date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find()->indexBy('client_id')->with('relationshipManager');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        // join with relation `relationshipManager` that is a relation to the table `person`
        // and set the table alias to be `relationshipManager`
        $query->joinWith('relationshipManager AS rm');
        
        $dataProvider->sort->attributes['relationshipManager.name'] = [
            'asc' => ['rm.name' => SORT_ASC],
            'desc' => ['rm.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'client_id' => $this->client_id,
            'hs_company_id' => $this->hs_company_id,
            'rm.person_id' => $this->relationship_manager_id,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
        ]);

        $query->andFilterWhere(['like', 'guid', $this->guid])
            ->andFilterWhere(['like', self::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', 'relationship_manager_id', $this->relationship_manager_id])
            ->andFilterWhere(['like', 'rm.name', $this->getAttribute('relationshipManager.name')]);

        return $dataProvider;
    }
}
