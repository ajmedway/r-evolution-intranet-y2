<?php

namespace app\models;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public static $superAdminID = 0;

    private static $users = [
        0 => [
            'id' => 0,
            'username' => 'admin',
            'password' => '9sd5sv',
            'authKey' => 'admin100key',
            'accessToken' => '100-token',
        ],
        10 => [
            'id' => 10,
            'username' => 'adam@r-evolution.co.uk',
            'password' => 'au88j5',
            'authKey' => 'admin101key',
            'accessToken' => '101-token',
        ],
        11 => [
            'id' => 11,
            'username' => 'gill@r-evolution.co.uk',
            'password' => 'u7u689',
            'authKey' => 'admin102key',
            'accessToken' => '102-token',
        ],
        12 => [
            'id' => 12,
            'username' => 'helen@r-evolution.co.uk',
            'password' => 'n94p3v',
            'authKey' => 'admin103key',
            'accessToken' => '103-token',
        ],
        7 => [
            'id' => 7,
            'username' => 'beccy@r-evolution.co.uk',
            'password' => '2x4bem',
            'authKey' => 'admin104key',
            'accessToken' => '104-token',
        ],
        1 => [
            'id' => 1,
            'username' => 'sam@r-evolution.co.uk',
            'password' => 'r47hn7',
            'authKey' => 'admin105key',
            'accessToken' => '105-token',
        ],
        2 => [
            'id' => 2,
            'username' => 'alec@r-evolution.co.uk',
            'password' => '00avenue',
            'authKey' => 'admin106key',
            'accessToken' => '106-token',
        ],
        3 => [
            'id' => 3,
            'username' => 'joe@r-evolution.co.uk',
            'password' => 'n224fj',
            'authKey' => 'admin107key',
            'accessToken' => '107-token',
        ],
        4 => [
            'id' => 4,
            'username' => 'byren@r-evolution.co.uk',
            'password' => '9m9y4d',
            'authKey' => 'admin108key',
            'accessToken' => '108-token',
        ],
        8 => [
            'id' => 8,
            'username' => 'ros@r-evolution.co.uk',
            'password' => '8pcw9b',
            'authKey' => 'admin109key',
            'accessToken' => '109-token',
        ],
        9 => [
            'id' => 9,
            'username' => 'steph@r-evolution.co.uk',
            'password' => 'ns6d47',
            'authKey' => 'admin110key',
            'accessToken' => '110-token',
        ],
        5 => [
            'id' => 5,
            'username' => 'anna@r-evolution.co.uk',
            'password' => 'x754ce',
            'authKey' => 'admin111key',
            'accessToken' => '111-token',
        ],
        19 => [
            'id' => 19,
            'username' => 'samantha@r-evolution.co.uk',
            'password' => '6rgd34',
            'authKey' => 'admin112key',
            'accessToken' => '112-token',
        ],
        20 => [
            'id' => 20,
            'username' => 'chris@r-evolution.co.uk',
            'password' => '4tdas7',
            'authKey' => 'admin113key',
            'accessToken' => '113-token',
        ],
        21 => [
            'id' => 21,
            'username' => 'danielle@r-evolution.co.uk',
            'password' => 'az80hy',
            'authKey' => 'admin114key',
            'accessToken' => '114-token',
        ],
        22 => [
            'id' => 22,
            'username' => 'connor@r-evolution.co.uk',
            'password' => 'el26bn',
            'authKey' => 'admin115key',
            'accessToken' => '115-token',
        ],
        23 => [
            'id' => 23,
            'username' => 'reza@r-evolution.co.uk',
            'password' => '7sh90m',
            'authKey' => 'admin115key',
            'accessToken' => '115-token',
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
