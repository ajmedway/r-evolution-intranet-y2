<?php

namespace app\models;

use Yii;
use app\components\helpers\Guid;

/**
 * This is the model class for table "person".
 *
 * @property int $person_id
 * @property string $guid
 * @property string $name
 * @property string $team
 * @property int $team_lead
 * @property int $active
 * @property string $date_created
 * @property string $date_modified
 *
 * @property Client[] $clients
 * @property Job[] $jobsProjectManager
 * @property Job[] $jobsCreatedBy
 * @property PersonOnJob[] $personOnJobs
 */
class Person extends \yii\db\ActiveRecord
{
    public static $TEAM_COMMS = 'Communications';
    public static $TEAM_DESIGN = 'Design';
    public static $TEAM_DIGITAL = 'Digital';
    public static $TEAM_OLM = 'OLM';
    public static $TEAM_PROJ_MMT = 'Project Management';
    public static $TEAM_OTHER = 'Other';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'person';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'team', 'active'], 'required'],
            [['person_id'], 'integer'],
            [['team_lead', 'active'], 'boolean'],
            [['date_created', 'date_modified'], 'safe'],
            [['guid'], 'string', 'max' => 32],
            [['name', 'team'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'person_id' => 'Person ID',
            'guid' => 'Guid',
            'name' => 'Name',
            'team' => 'Team',
            'team_lead' => 'Team Lead',
            'active' => 'Active',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }
    
    /**
     * @param bool return values as keys
     * @return array Teams
     */
    public static function teams($valuesAsKeys = false)
    {
        $data = [
            self::$TEAM_COMMS,
            self::$TEAM_DESIGN,
            self::$TEAM_DIGITAL,
            self::$TEAM_OLM,
            self::$TEAM_PROJ_MMT,
            self::$TEAM_OTHER,
        ];
        
        return $valuesAsKeys ? array_combine($data, $data) : $data;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['relationship_manager_id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobsProjectManager()
    {
        return $this->hasMany(Job::className(), ['project_manager_id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobsCreatedBy()
    {
        return $this->hasMany(Job::className(), ['created_by_id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonOnJobs()
    {
        return $this->hasMany(PersonOnJob::className(), ['person_id' => 'person_id']);
    }

    /**
     * @return bool Is a team person
     */
    public function isTeam()
    {
        return strpos($this->name, 'Team') !== false;
    }

    /**
     * {@inheritdoc}
     * @return PersonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PersonQuery(get_called_class());
    }
    
    /**
     * Handle any nuances of data in the model before we commit to insert/update
     * 
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_modified = new \yii\db\Expression('NOW()');
            if (empty($this->date_created)) {
                $this->date_created = new \yii\db\Expression('NOW()');
            }
            if (empty($this->guid)) {
                $this->guid = Guid::generateGuid();
            }
            return true;
        } else {
            return false;
        }
    }
}
