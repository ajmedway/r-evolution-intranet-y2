<?php

namespace app\models;

use Yii;
use app\components\helpers\Guid;

/**
 * This is the model class for table "client".
 *
 * @property int $client_id
 * @property string $guid
 * @property int $hs_company_id
 * @property int $relationship_manager_id
 * @property string $name
 * @property string $date_created
 * @property string $date_modified
 *
 * @property Person $relationshipManager
 * @property Job[] $jobs
 */
class Client extends \yii\db\ActiveRecord
{
    public static $REVS_ID = 93;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['relationship_manager_id', 'name'], 'required'],
            [['hs_company_id', 'relationship_manager_id'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
            [['guid'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 255],
            [['name', 'hs_company_id'], 'unique'],
            [['relationship_manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['relationship_manager_id' => 'person_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Client ID',
            'guid' => 'Guid',
            'hs_company_id' => 'Hubspot Company ID',
            'relationship_manager_id' => 'Relationship Manager ID',
            'name' => 'Name',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelationshipManager()
    {
        return $this->hasOne(Person::className(), ['person_id' => 'relationship_manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Job::className(), ['client_id' => 'client_id']);
    }

    /**
     * {@inheritdoc}
     * @return ClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientQuery(get_called_class());
    }
    
    /**
     * Handle any nuances of data in the model before we commit to insert/update
     * 
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_modified = new \yii\db\Expression('NOW()');
            if (empty($this->date_created)) {
                $this->date_created = new \yii\db\Expression('NOW()');
            }
            if (empty($this->guid)) {
                $this->guid = Guid::generateGuid();
            }
            return true;
        } else {
            return false;
        }
    }
    
}
