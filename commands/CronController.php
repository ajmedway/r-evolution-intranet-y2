<?php
/**
 * Copyright © 2018 r//evolution Marketing. All rights reserved.
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\Client;
use app\models\Job;
use app\models\Person;
use app\models\PersonOnJob;

/**
 * This is the "command central" for console commands to be run from crontab.
 */
class CronController extends Controller
{
    /**
     * This command checks for newly close/won Hubspot deals, which are parsed
     * into Job records, with associated personOnJob records also created.
     * 
     * Command: ./yii cron/import-hubspot-deals
     * 
     * @return string The completion message to be echoed.
     */
    public function actionImportHubspotDeals()
    {
        $hubspot = Yii::$app->hubspotDataManager;

        $allDeals = $hubspot->getAllDeals();
        if (count($allDeals) === 0) {
            $message = 'No deals in Hubspot portal.';
            Yii::info(['message' => $message], __METHOD__);
            exit($message . PHP_EOL);
        }

        // tracker for total valid deals to be imported
        $validDeals = 0;
        // tracker for new jobs created
        $newJobIds = [];

        foreach ($allDeals as $deal) {
            // skip over deals that are not in closed/won stage
            if ($hubspot->pVal($deal, 'dealstage') !== 'closedwon') {
                continue;
            }
            // skip over deals already in the dashboard / where in_dashboard property not explicitly set
            if (in_array($hubspot->pVal($deal, 'in_dashboard'), [true, null], true)) {
                continue;
            }
            // skip over deals that do not have a valid dealId value or are already parsed into jobs
            if (!isset($deal->dealId) || !is_numeric($deal->dealId) || Job::find()->where(['hs_deal_id' => (int) $deal->dealId])->count() > 0) {
                continue;
            }
            $validDeals++;

            // inspect associated companies of this deal
            if (isset($deal->associations->associatedCompanyIds) && is_array($deal->associations->associatedCompanyIds)) {
                foreach ($deal->associations->associatedCompanyIds as $hsCompanyId) {
                    $clientQuery = Client::find()->where(['hs_company_id' => $hsCompanyId]);
                    $hsCompany = $hubspot->getCompanyById($hsCompanyId);
                    if (!$clientQuery->exists() && !empty($hsCompany)) {
                        $client = new Client();
                        $client->hs_company_id = $hsCompanyId;
                        $client->relationship_manager_id = 0;
                        $client->name = $hubspot->pVal($hsCompany, 'name');
                        $client->save(false);
                    } else {
                        $client = $clientQuery->one();
                    }
                }
            }

            // get the expected billing date
            $expectedBillingDate = $hubspot->pVal($deal, 'expected_billing_date');
            // check the allocations add up to the total and also a non-zero value
            if (empty($expectedBillingDate)) {
                Yii::warning(['message' => "The `expected_billing_date` field is not set for Hubspot dealId: {$deal->dealId}"], __METHOD__);
                continue;
            }
            $billingDT = new \DateTime();
            $billingDT->setTimestamp(substr($expectedBillingDate, 0, 10));
            $billingDT->setTimezone(new \DateTimeZone(Yii::$app->timeZone));

            // correct for dates over 28th of a month, the effective limit for using month intervals
            // see "Example #3 Beware when adding months" here: https://secure.php.net/manual/en/datetime.add.php
            if ($billingDT->format('d') > 28) {
                $billingDT->setDate($billingDT->format('Y'), $billingDT->format('m'), 28);
            }

            // determine if split across multiple jobs
            $numberBillingMonths = (int) $hubspot->pVal($deal, 'number_of_billing_months') ?: 1;

            // get team allocations
            $allocations = [
                Person::$TEAM_COMMS => (float) $hubspot->pVal($deal, 'comms_allocation'),
                Person::$TEAM_DESIGN => (float) $hubspot->pVal($deal, 'design_allocation'),
                Person::$TEAM_DIGITAL => (float) $hubspot->pVal($deal, 'digital_allocation'),
                Person::$TEAM_OLM => (float) $hubspot->pVal($deal, 'olm_allocation'),
            ];

            // calculate team allocations total
            $totalAmount = (float) array_sum($allocations);
            // get the total amount entered for the deal
            $amount = (float) $hubspot->pVal($deal, 'amount');
            // check the allocations add up to the total and also a non-zero value
            if (empty($totalAmount) || $totalAmount !== $amount) {
                Yii::warning(['message' => "The sum of the team allocations does not match the deal total, or is zero, for Hubspot dealId: {$deal->dealId}"], __METHOD__);
                continue;
            }
            
            // get the buy-in cost
            $inCost = (float) $hubspot->pVal($deal, 'buy_in_allocation');
            // use the standard day rate
            $quoteRate = 600;

            // get team leader Person instances
            $teamLeads = [
                Person::$TEAM_COMMS => Person::find()->teamLead(Person::$TEAM_COMMS)->one(),
                Person::$TEAM_DESIGN => Person::find()->teamLead(Person::$TEAM_DESIGN)->one(),
                Person::$TEAM_DIGITAL => Person::find()->teamLead(Person::$TEAM_DIGITAL)->one(),
                Person::$TEAM_OLM => Person::find()->teamLead(Person::$TEAM_OLM)->one(),
            ];

            for ($i = 1; $i <= $numberBillingMonths; $i++) {
                // get the chunked month amounts
                $monthAmount = $totalAmount / $numberBillingMonths;
                $monthInCost = $inCost / $numberBillingMonths;
                $monthUnits = $monthAmount / $quoteRate;
                $monthMargin = $monthAmount - $monthInCost;

                // calculate the team unit allocations
                $monthUnitAllocations = [
                    Person::$TEAM_COMMS => empty($allocations[Person::$TEAM_COMMS]) ? 0 : $monthAmount / ($totalAmount / $allocations[Person::$TEAM_COMMS]) / $quoteRate,
                    Person::$TEAM_DESIGN => empty($allocations[Person::$TEAM_DESIGN]) ? 0 : $monthAmount / ($totalAmount / $allocations[Person::$TEAM_DESIGN]) / $quoteRate,
                    Person::$TEAM_DIGITAL => empty($allocations[Person::$TEAM_DIGITAL]) ? 0 : $monthAmount / ($totalAmount / $allocations[Person::$TEAM_DIGITAL]) / $quoteRate,
                    Person::$TEAM_OLM => empty($allocations[Person::$TEAM_OLM]) ? 0 : $monthAmount / ($totalAmount / $allocations[Person::$TEAM_OLM]) / $quoteRate,
                ];

                // build the new job data payload
                $jobData = [
                    'hs_deal_id' => (int) $deal->dealId,
                    'client_id' => !empty($client->client_id) ? $client->client_id : Client::$REVS_ID, // defaults to r//evolution
                    'project_manager_id' => (int) $hubspot->pVal($deal, 'project_manager'),
                    'created_by_id' => \app\models\User::$superAdminID,
                    'job_name' => $hubspot->pVal($deal, 'dealname') . " (Part {$i} of {$numberBillingMonths}: {$billingDT->format('F')})",
                    'quote_rate' => 600,
                    'quote_unit' => 'Day',
                    'total_units' => $monthUnits,
                    'total_value' => $monthAmount,
                    'sale_total_value' => $monthAmount,
                    'in_cost' => $monthInCost,
                    'margin' => $monthMargin,
                    'state' => 'Confirmed',
                    'expected_billing_date' => $billingDT->format('Y-m-d'),
                ];

                // create the job record
                $job = new Job();
                $job->loadDefaultValues();
                if ($job->load($jobData, '') && $job->save(false)) {
                    foreach ($monthUnitAllocations as $team => $unitAllocation) {
                        // skip over teams with no allocation
                        if (empty($unitAllocation)) {
                            continue;
                        }
                        // create the personOnJob records
                        $personOnJob = new PersonOnJob();
                        $personOnJob->job_id = $job->job_id;
                        $personOnJob->person_id = !empty($teamLeads[$team]->person_id) ?
                            $teamLeads[$team]->person_id :
                            \app\models\User::$superAdminID;
                        $personOnJob->time_units = $unitAllocation;
                        $personOnJob->save(false);
                    }

                    // update Hubspot deal as in_dashboard = true
                    $hubspot->updateDealById($deal->dealId, [
                        [
                            'name' => 'in_dashboard',
                            'value' => true,
                        ]
                    ]);
                    
                    // add new job to the tracker array
                    $newJobIds[] = $job->job_id;
                } else {
                    Yii::error(['message' => "Unable to load/save Job record for Hubspot dealId: {$deal->dealId}", '$jobData' => $jobData], __METHOD__);
                }

                // add a month onto the billing date for the next iteration
                $billingDT->add(new \DateInterval('P1M'));
            }
        }

        // finalise with info logging and exit message
        $totalNewJobs = count($newJobIds);
        $completionMessage = "Created {$totalNewJobs} Job records from {$validDeals} closed/won Hubspot deals." . PHP_EOL . 'DONE.' . PHP_EOL;
        Yii::info(['message' => $completionMessage, '$newJobIds' => $newJobIds], __METHOD__);
        exit($completionMessage);
    }
}
