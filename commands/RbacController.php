<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        
        
        ### Client Model Permissions ###
        
        // add "createClient" permission
        $createClient = $auth->createPermission('createClient');
        $createClient->description = 'Create a client';
        $auth->add($createClient);

        // add "updateClient" permission
        $updateClient = $auth->createPermission('updateClient');
        $updateClient->description = 'Update client';
        $auth->add($updateClient);

        // add "deleteClient" permission
        $deleteClient = $auth->createPermission('deleteClient');
        $deleteClient->description = 'Delete client';
        $auth->add($deleteClient);
        
        
        ### Job Model Permissions ###
        
        // add "createJob" permission
        $createJob = $auth->createPermission('createJob');
        $createJob->description = 'Create a job';
        $auth->add($createJob);

        // add "updateJob" permission
        $updateJob = $auth->createPermission('updateJob');
        $updateJob->description = 'Update job';
        $auth->add($updateJob);

        // add "deleteJob" permission
        $deleteJob = $auth->createPermission('deleteJob');
        $deleteJob->description = 'Delete job';
        $auth->add($deleteJob);
        
        
        ### Person Model Permissions ###
        
        // add "createPerson" permission
        $createPerson = $auth->createPermission('createPerson');
        $createPerson->description = 'Create a person';
        $auth->add($createPerson);

        // add "updatePerson" permission
        $updatePerson = $auth->createPermission('updatePerson');
        $updatePerson->description = 'Update person';
        $auth->add($updatePerson);

        // add "deletePerson" permission
        $deletePerson = $auth->createPermission('deletePerson');
        $deletePerson->description = 'Delete person';
        $auth->add($deletePerson);
        
        
        ### Setup Roles ###

        // add "user" role and give this role the "createJob" and "updateJob" permissions
        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $createClient);
        $auth->addChild($user, $updateClient);
        $auth->addChild($user, $createJob);
        $auth->addChild($user, $updateJob);
        $auth->addChild($user, $deleteJob);

        // add "admin" role and give this role the permissions of the "user" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $user);
        
        // as well as these extra permissions
        $auth->addChild($admin, $deleteClient);
        $auth->addChild($admin, $createPerson);
        $auth->addChild($admin, $updatePerson);
        $auth->addChild($admin, $deletePerson);

        
        ### Assign roles to users ###
        
        /*
         * Users
         * 
         * '0','admin',
         * '2','Alec Pritchard'
         * '10','Adam Blenkinsop'
         * '11','Gill Burgess'
         * '12','Helen Hardy'
         * 
         * '1','Sam Leaver'
         * '3','Joe Jackson'
         * '4','Byren Atkinson'
         * '5','Anna Graham'
         * '7','Beccy Gregory'
         * '8','Ros Haverson'
         * '9','Steph Hudson'
         * '19','Samantha Wilcox'
         * '20','Chris Glancey'
         * '21','Danielle Woods'
         * '22','Connor Henderson'
         * '23','Reza Badel'
         */
        
        // Second args are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($admin, 0);
        $auth->assign($admin, 2);
        $auth->assign($admin, 10);
        $auth->assign($admin, 11);
        $auth->assign($admin, 12);
        
        $auth->assign($user, 1);
        $auth->assign($user, 3);
        $auth->assign($user, 4);
        $auth->assign($user, 5);
        $auth->assign($user, 7);
        $auth->assign($user, 8);
        $auth->assign($user, 9);
        $auth->assign($user, 19);
        $auth->assign($user, 20);
        $auth->assign($user, 21);
        $auth->assign($user, 22);
        $auth->assign($user, 23);
        
        $this->stdout('All done!' . PHP_EOL);
    }
}
